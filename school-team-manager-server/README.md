# School Team Manager API
### by Morten Svedjan Dahl, Håkon Dalen Hestnes, Michael Mellemseter and Jakkris Thongma

An API for the School Team Manager. The API was created by creating a Spring Boot project,
using Spring Data for data access and Hibernate for object-relational mapping (ORM).

You can access the API at https://school-team-manager.herokuapp.com/, but you can also clone this project and run it locally.

## How to setup
This project needs to be connected to a database, and to connect to your own database you need to make your own application.properties file.
This should be located at src/main/resources/application.properties and contain the right logins for your database.
The application.properties file also requires a connection to Keycloak to configure the security, and a connection to a mail which wil be the
mail that sends the login informations to new users.

To configure the connection to Keycloak you can copy this into your application.properties file:  
```java
spring.security.oauth2.resourceserver.jwt.issuer-uri= https://keycloak-school-team-managerv2.herokuapp.com/auth/realms/School-team-manager
spring.security.oauth2.resourceserver.jwt.jwk-set-uri= https://keycloak-school-team-managerv2.herokuapp.com/auth/realms/School-team-manager/protocol/openid-connect/certs
```

## How to run
To run the project locally open the project and run it in an IDE.

## API Documentation
This project is using Swagger.io for API documentation. The documentation is accessible at
https://school-team-manager.herokuapp.com/swagger-ui.html or at http://localhost:8080/swagger-ui.html
if you are running the project locally.
