package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="player")
public class Player{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "show_personalia")
    private Boolean showPersonalia;

    @Column(name = "show_photo")
    private Boolean showPhoto;

    @Column(name = "show_medical_notes")
    private Boolean showMedicalNotes;

    @Column(name = "medical_notes")
    private String medicalNotes;

    @Column(name = "profile_picture")
    private String profilePicture;

    @ManyToMany
    @JoinTable(
            name="player_parent",
            joinColumns={@JoinColumn(name="player_id")},
            inverseJoinColumns={@JoinColumn(name="parent_id")}
    )
    public Set<Parent> parents;

    @OneToMany(mappedBy = "player", cascade = CascadeType.REMOVE)
    private Set<TeamPlayer> teamPlayers;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    public User user;

    public Player() { }

    public Player(Long id, Boolean showMedicalNotes, Boolean showPersonalia , Boolean showPhoto, String medicalNotes, String profilePicture, Set<Parent> parents, Set<TeamPlayer> teamPlayers, User user) {
        this.id = id;
        this.showMedicalNotes = showMedicalNotes;
        this.showPersonalia = showPersonalia;
        this.showPhoto = showPhoto;
        this.medicalNotes = medicalNotes;
        this.profilePicture = profilePicture;
        this.parents = parents;
        this.teamPlayers = teamPlayers;
        this.user = user;
    }

    public String getMedicalNotes() {
        return medicalNotes;
    }

    public void setMedicalNotes(String medicalNotes) {
        this.medicalNotes = medicalNotes;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Boolean isShowPersonalia() {
        return showPersonalia;
    }

    public void setShowPersonalia(Boolean showPersonalia) {
        this.showPersonalia = showPersonalia;
    }

    public Boolean isShowPhoto() {
        return showPhoto;
    }

    public void setShowPhoto(Boolean showPhoto) {
        this.showPhoto = showPhoto;
    }

    public Boolean isShowMedicalNotes() {
        return showMedicalNotes;
    }

    public void setShowMedicalNotes(Boolean showMedicalNotes) {
        this.showMedicalNotes = showMedicalNotes;
    }

    public Long getId() {
        return id;
    }

    public void setUser(User user) { this.user = user; }

    public User getUser() {return user; }

    public Set<TeamPlayer> getTeamPlayers() {
        return teamPlayers;
    }

    public Set<Parent> getParents() { return parents; }

    public void setParents(Set<Parent> parents) { this.parents = parents; }
}
