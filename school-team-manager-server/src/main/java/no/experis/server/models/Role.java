package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "role_name")
    private String roleName;

    @Column (name = "keycloak_id")
    private String keycloakId;

    @OneToMany(mappedBy="role")
    public Set<User> users;

    @JsonGetter("users")
    public List<String> usersGetter() {
        if(users != null) {
            return users.stream()
                    .map(user -> {
                        return "/api/v1/users/" + user.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Role() { }

    public Role(Long id, String roleName, Set<User> users) {
        this.id = id;
        this.roleName = roleName;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public String getRoleName() { return roleName; }

    public String getKeycloakId() { return keycloakId; }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public void setKeycloakId(String keycloakId) {
        this.keycloakId = keycloakId;
    }
}
