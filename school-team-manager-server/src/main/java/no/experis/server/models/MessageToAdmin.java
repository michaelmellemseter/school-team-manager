package no.experis.server.models;

import javax.persistence.*;

@Entity
@Table(name = "message_to_admin")
public class MessageToAdmin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "message")
    private String message;

    @Column(name = "approved")
    private Boolean approved;

    @ManyToOne
    @JoinColumn(name = "users")
    public User user;

    public MessageToAdmin() {

    }

    public MessageToAdmin(Long id, String title, String message, Boolean approved, User user) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.approved = approved;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Boolean getApproved() {
        return approved;
    }
}
