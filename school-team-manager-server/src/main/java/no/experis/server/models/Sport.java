package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="sport")
public class Sport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @OneToMany(mappedBy ="sport")
    public Set<Team> teams;

    @JsonGetter("teams")
    public List<String> teamsGetter() {
        if(teams != null) {
            return teams.stream()
                    .map(team -> {
                        return "api/v1/teams/" + team.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Sport() {

    }

    public Sport(Long id, String name, Set<Team> teams) {
        this.id = id;
        this.name = name;
        this.teams = teams;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
