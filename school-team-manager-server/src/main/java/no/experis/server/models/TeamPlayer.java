package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;

@Entity
@Table(name="team_player")
public class TeamPlayer {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "player_id")
    public Player player;
    @JsonGetter("player")
    public String playerJsonGetter() {
        if(team != null) {
            return "/api/v1/players/" + player.getId();
        }
        return null;
    }



    @ManyToOne(optional = false)
    @JoinColumn(name="team_id")
    public Team team;
    @JsonGetter("team")
    public String teamJsonGetter() {
        if(team != null) {
            return "/api/v1/teams/" + team.getId();
        }
        return null;
    }

    @Column(name = "jersey_number")
    public int jerseyNumber;

    public TeamPlayer() {
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }
}


// (player) 1  m (team_player) m   1 (team)
//