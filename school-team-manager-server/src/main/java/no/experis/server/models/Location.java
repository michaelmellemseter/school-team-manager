package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(
            name="location_location_type",
            joinColumns = {@JoinColumn(name="location_id")},
            inverseJoinColumns = {@JoinColumn(name="location_type_id")}
    )
    public Set<LocationType> locations;

    @ManyToMany(mappedBy = "locations")
    public Set<School> schools;

    @JsonGetter("schools")
    public List<String> schoolsGetter() {
        if (schools != null) {
            return schools.stream()
                    .map(school -> {
                        return "/api/v1/schools/" + school.getId();
                    }).collect((Collectors.toList()));
        }
        return null;
    }

    @OneToMany(mappedBy = "location")
    public Set<Match> matches;

    @JsonGetter("matches")
    public List<String> matchesGetter() {
        if(matches != null) {
            return matches.stream()
                    .map(match -> {
                        return "api/v1/matches/" + match.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @OneToMany(mappedBy = "location")
    public Set<Team> teams;

    @JsonGetter("teams")
    public List<String> teamsGetter() {
        if(teams != null) {
            return teams.stream()
                    .map(team -> {
                        return "api/v1/teams/" + team.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Location() {

    }

    public Location(Long id, String name, Set<School> schools, Set<LocationType> locations, Set<Match> matches, Set<Team> teams) {
        this.id = id;
        this.name = name;
        this.schools = schools;
        this.locations = locations;
        this.matches = matches;
        this.teams = teams;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<LocationType> getLocations() {
        return locations;
    }

    public void setLocations(Set<LocationType> locations) {
        this.locations = locations;
    }
}
