package no.experis.server.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="admin")
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    public User user;

    public Admin(Long id) {
        this.id = id;
    }

    public Admin() {

    }

    public Long getId() {
        return id;
    }


    public void setUser(User user) { this.user = user; }

    public User getUser() { return user; }
}
