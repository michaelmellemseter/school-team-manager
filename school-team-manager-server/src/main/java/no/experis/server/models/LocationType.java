package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "location_type")
public class LocationType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "locations")
    public Set<Location> locations;

    @JsonGetter("locations")
    public List<String> locationsGetter() {
        if (locations != null) {
            return locations.stream()
                    .map(location -> {
                        return "/api/v1/locations/" + location.getId();
                    }).collect((Collectors.toList()));
        }
        return null;
    }

    public LocationType() {

    }

    public LocationType(Long id, String name, Set<Location> locations) {
        this.id = id;
        this.name = name;
        this.locations = locations;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
