package no.experis.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="parent")
public class Parent{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToMany()
    @JoinTable(
            name="player_parent",
            joinColumns={@JoinColumn(name="parent_id")},
            inverseJoinColumns={@JoinColumn(name="player_id")}
    )
    public Set<Player> players;

    @JsonGetter("players")
    public List<String> playerGetter() {
        if(players != null) {
            return players.stream()
                    .map(player -> {
                        return "/api/v1/players/" + player.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    //user
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    public User user;

    public Parent(Long id, Set<Player> players, Long id1, User user) {
        this.id = id;
        this.players = players;
        this.id = id1;
        this.user = user;
    }


    public Parent(){ }

    public Long getId() { return id; }

    public void setUser(User user) { this.user = user; }

    public User getUser() { return user; }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }
}
