package no.experis.server.models;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name="match")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="date")
    private Date date;

    @Column(name="time")
    private LocalTime time;

    @Column(name="canceled_reason")
    private String canceledReason;

    @Column(name="home_score")
    private Integer homeScore;

    @Column(name="away_score")
    private Integer awayScore;

    @ManyToOne
    @JoinColumn(name = "location_id")
    public Location location;

    // This relation is NOT optional as a match must have a home team
    @ManyToOne(optional = false)
    @JoinColumn(name = "home_team_id", referencedColumnName = "id")
    public Team homeTeam;

    // This relation is NOT optional as a match must have a away team
    @ManyToOne(optional = false)
    @JoinColumn(name = "away_team_id", referencedColumnName = "id")
    public Team awayTeam;

    public Match() {

    }

    public Match(Long id, Date date, LocalTime time, String canceledReason, Integer homeScore, Integer awayScore, Location location, Team homeTeam, Team awayTeam) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.canceledReason = canceledReason;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.location = location;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public Long getId() { return id; }

    public Date getDate() { return date; }

    public void setDate(Date date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getCanceledReason() { return canceledReason; }

    public void setCanceledReason(String canceledReason) {
        this.canceledReason = canceledReason;
    }

    public Integer getHomeScore(){ return homeScore; }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public Integer getAwayScore() { return awayScore; }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }


}
