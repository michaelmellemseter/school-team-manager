package no.experis.server.models.dto;

public class TeamPlayerDTO
{
    public Long playerId;
    public int jerseyNr;

    public Long getPlayerId() {
        return playerId;
    }

    public int getJerseyNr() {
        return jerseyNr;
    }
}
