package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.LocationType;
import no.experis.server.models.MessageToAdmin;
import no.experis.server.repositories.MessageToAdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/admins/messages")
public class MessageToAdminController {

    @Autowired
    private MessageToAdminRepository messageToAdminRepository;

    @Operation(summary = "Get all messages to admins from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all messages to admins",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = MessageToAdmin.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<List<MessageToAdmin>> getAllMessagesToAdmins() {
        List<MessageToAdmin> messagesToAdmins = messageToAdminRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(messagesToAdmins, resp);
    }

    @Operation(summary = "Get all messages to admins that's not corrected from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all messages to admins that are not approved",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = MessageToAdmin.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping("/active")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<List<MessageToAdmin>> getAllMessagesToAdminsNotApproved() {
        List<MessageToAdmin> messagesToAdmins = messageToAdminRepository.findAllByApprovedIsNull();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(messagesToAdmins, resp);
    }

    @Operation(summary = "Get a message to admin by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the message to admin",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MessageToAdmin.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message to admin not found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<MessageToAdmin> getMessageToAdminById(@Parameter(description = "Id of message to admin to be searched") @PathVariable Long id) {
        MessageToAdmin returnMessageToAdmin = new MessageToAdmin();
        HttpStatus status;
        if (messageToAdminRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            returnMessageToAdmin = messageToAdminRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMessageToAdmin, status);
    }

    @Operation(summary = "Update a message to admin by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the message to admin",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MessageToAdmin.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message to admin not found",
                    content = @Content)
    })
    @CrossOrigin
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<MessageToAdmin> updateMessageToAdmin(@Parameter(description = "Id of message to admin to be updated") @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The updated message to admin", required = true, content = @Content(
            schema = @Schema(implementation = MessageToAdmin.class))) @RequestBody MessageToAdmin MessageToAdmin) {
        HttpStatus status;
        if(messageToAdminRepository.findById(id).isPresent()){
            MessageToAdmin returnMessageToAdmin = new MessageToAdmin();

            if (!id.equals(MessageToAdmin.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(returnMessageToAdmin, status);
            }

            returnMessageToAdmin = messageToAdminRepository.save(MessageToAdmin);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(returnMessageToAdmin, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }

    @Operation(summary = "Create a message to admin", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new message to admin",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MessageToAdmin.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @PostMapping
    @PreAuthorize("hasRole('Admin') or hasRole('Coach') or hasRole('Coach') or hasRole('Player')")
    public ResponseEntity<MessageToAdmin> addMessageToAdmin(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The new message to admin", required = true, content = @Content(
            schema = @Schema(implementation = MessageToAdmin.class))) @RequestBody MessageToAdmin MessageToAdmin) {
        MessageToAdmin returnMessageToAdmin = messageToAdminRepository.save(MessageToAdmin);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMessageToAdmin, status);
    }

    @Operation(summary = "Approve a message to admin by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Approved the message to admin",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MessageToAdmin.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message to admin not found",
                    content = @Content)
    })
    @CrossOrigin
    @PatchMapping("/{id}/approve")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<MessageToAdmin> updateMessageToAdminApproved(@Parameter(description = "Id of message to admin to be approved") @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The approved message to admin", required = true, content = @Content(
            schema = @Schema(implementation = MessageToAdmin.class))) @RequestBody MessageToAdmin MessageToAdmin) {
        HttpStatus status;
        if(messageToAdminRepository.findById(id).isPresent())
        {
            MessageToAdmin returnMessageToAdmin = messageToAdminRepository.findById(id).get();
            if (!id.equals(MessageToAdmin.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(returnMessageToAdmin, status);
            }
            returnMessageToAdmin.setApproved(MessageToAdmin.getApproved());
            messageToAdminRepository.save(returnMessageToAdmin);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(returnMessageToAdmin, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }
}
