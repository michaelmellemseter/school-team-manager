package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.Location;
import no.experis.server.models.Match;
import no.experis.server.models.Team;
import no.experis.server.repositories.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping(value="api/v1/matches")
public class MatchController {
    @Autowired
    private MatchRepository matchRepository;

    @Operation(summary = "Get all matches", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all matches",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Match.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<List<Match>> getAllMatches() {
        List<Match> matches = matchRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(matches, resp);
    }

    @Operation(summary = "Get all matches that has an upcoming date", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all matches",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Match.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping("/upcoming")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach') or hasRole('Parent') or hasRole('Player')")
    public ResponseEntity<List<Match>> getAllUpcomingMatches(@RequestBody (required = false) Team[] teams) {
        Date now = new Date();
        List<Match> matches;
        if (teams == null) {
            matches = matchRepository.findByDateGreaterThanEqualOrderByDate(now);
        } else {
            Set<Long> ids = new HashSet<>();
            for (Team team : teams) {
                ids.add(team.getId());
            }
            matches = matchRepository.findByHomeTeamIdInOrAwayTeamIdInAndDateGreaterThanEqualOrderByDate(ids, ids, now);
        }
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(matches, resp);
    }

    @Operation(summary = "Get all matches that has a earlier date than today", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all matches",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Match.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping("/finished")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach') or hasRole('Parent') or hasRole('Player')")
    public ResponseEntity<List<Match>> getAllFinishedMatches(@RequestBody (required = false) Team[] teams) {
        Date now = new Date();
        List<Match> matches;
        if (teams == null) {
            matches = matchRepository.findByDateLessThanEqualAndCanceledReasonIsNullOrderByDateDesc(now);
        } else {
            Set<Long> ids = new HashSet<>();
            for (Team team : teams) {
                ids.add(team.getId());
            }
            matches = matchRepository.findByHomeTeamIdInOrAwayTeamIdInAndDateLessThanEqualAndCanceledReasonIsNullOrderByDateDesc(ids, ids, now);
        }
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(matches, resp);
    }

    @Operation(summary = "Get a match by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the match",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Match not found",
                    content = @Content) })

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Match> getMatchById(@PathVariable Long id) {
        Match returnMatch = new Match();
        HttpStatus resp;

        if(matchRepository.findById(id).isPresent()) {
            System.out.println("Found match with id: " + id);
            returnMatch = matchRepository.findById(id).get();
            resp = HttpStatus.OK;
        } else {
            System.out.println("Match with id " + id + " not found");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMatch, resp);
    }

    @Operation(summary = "Add a match", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Added a match",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class)) }),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @PostMapping
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Match> addNewMatch(@RequestBody Match match) {
        Match returnMatch = matchRepository.save(match);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMatch, status);
    }

    @Operation(summary = "Update a match", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the match",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Match> updateMatch(@PathVariable Long id, @RequestBody Match match) {
        Match returnMatch = new Match();
        HttpStatus status;

        if(!id.equals(match.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMatch, status);
        }

        returnMatch = matchRepository.save(match);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnMatch, status);
    }

    @Operation(summary = "Delete a match by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "The match was deleted",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Match not found",
                    content = @Content)
    })
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('Admin')")
    public HttpStatus deleteMatch(@PathVariable Long id) {
        HttpStatus status;
        if(matchRepository.findById(id).isPresent()){
            Match deleteMatch = matchRepository.findById(id).get();
            // Remove the match from the lists in both the teams
            Team homeTeam = deleteMatch.homeTeam;
            Team awayTeam = deleteMatch.awayTeam;
            if(homeTeam != null) homeTeam.homeMatches.remove(deleteMatch);
            if(awayTeam != null) awayTeam.awayMatches.remove(deleteMatch);
            status = HttpStatus.NO_CONTENT;
            matchRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

    @Operation(summary = "Update the score for a match", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the match",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class))}),
            @ApiResponse(responseCode = "400", description = "Id in body does not match path id",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Match not found",
                    content = @Content)
    })
    @CrossOrigin
    @PatchMapping("/{id}/score")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Match> updateMatchScore(@Parameter(description = "Id of match to be updated") @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The two scores with the id of the match", required = true, content = @Content(
            schema = @Schema(implementation = Location.class))) @RequestBody Match match) {
        HttpStatus status;
        if(matchRepository.findById(id).isPresent())
        {
            Match returnMatch = matchRepository.findById(id).get();
            if(!id.equals(match.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(returnMatch, status);
            }

            returnMatch.setHomeScore(match.getHomeScore());
            returnMatch.setAwayScore(match.getAwayScore());
            matchRepository.save(returnMatch);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(returnMatch, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }

    @Operation(summary = "Update the canceled reason for a match", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the match",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class)) }),
            @ApiResponse(responseCode = "400", description = "Id in body does not match path id",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Match not found",
                    content = @Content)
    })
    @CrossOrigin
    @PatchMapping("/{id}/cancel")
    @PreAuthorize("hasRole('Admin') or hasRole('coach')")
    public ResponseEntity<Match> updateMatchCanceledReason(@Parameter(description = "Id of match to be updated") @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The canceled reason with the id of the canceled match", required = true, content = @Content(
            schema = @Schema(implementation = Location.class))) @RequestBody Match match) {
        HttpStatus status;
        if(matchRepository.findById(id).isPresent()){
            Match returnMatch = matchRepository.findById(id).get();
            if(!id.equals(match.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(returnMatch, status);
            }
            returnMatch.setCanceledReason(match.getCanceledReason());
            matchRepository.save(returnMatch);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(returnMatch, status);
        } else {
          status = HttpStatus.NOT_FOUND;
          return new ResponseEntity<>(status);
        }
    }
}
