package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.School;
import no.experis.server.models.Sport;
import no.experis.server.models.Team;
import no.experis.server.repositories.SportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1/sports")
public class SportController {
    @Autowired
    private SportRepository sportRepository;
    @Operation(summary = "Get all sports", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all sports",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Sport.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<List<Sport>> getAllSports() {
        List<Sport> sports = sportRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(sports, resp);
    }

    @Operation(summary = "Get a sport by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the sport",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Sport.class)) }),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Sport not found",
                    content = @Content) })

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Sport> getSportById(@PathVariable Long id) {
        Sport returnSport = new Sport();
        HttpStatus resp;

        if(sportRepository.findById(id).isPresent()) {
            System.out.println("Found sport with id: " + id);
            returnSport = sportRepository.findById(id).get();
            resp = HttpStatus.OK;
        } else {
            System.out.println("Sport with id " + id + " not found");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnSport, resp);
    }

    @Operation(summary = "Add a sport", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Added a sport",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Sport.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @PostMapping
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Sport> addNewSport(@RequestBody Sport sport) {
        Sport returnSport = sportRepository.save(sport);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnSport, status);
    }

    @Operation(summary = "Update a sport", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the sport",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Sport.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location not found",
                    content = @Content),
    })
    @CrossOrigin
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Sport> updateSport(@PathVariable Long id, @RequestBody Sport sport) {
        Sport returnSport = new Sport();
        HttpStatus status;

        if(!id.equals(sport.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnSport, status);
        }
        if(sportRepository.findById(id).isPresent())
        {
            returnSport = sportRepository.save(sport);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnSport, status);
    }

    @Operation(summary = "Delete a sport by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "The sport was deleted",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Sport.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Sport not found",
                    content = @Content) })
    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('Admin')")
    public HttpStatus deleteSport(@PathVariable Long id) {
        HttpStatus status;
        if(sportRepository.findById(id).isPresent()){
            Sport deleteSport = sportRepository.findById(id).get();
            // Remove the sport from teams that have it
            Set<Team> teams = deleteSport.teams;
            for(Team team : teams) team.sport = null;
            // Remove the sport
            sportRepository.deleteById(id);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }
}
