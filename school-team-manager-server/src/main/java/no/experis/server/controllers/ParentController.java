package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.*;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import no.experis.server.repositories.MatchRepository;
import no.experis.server.repositories.ParentRepository;
import no.experis.server.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/parents")
public class ParentController {
    @Autowired
    ParentRepository parentRepository;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    MatchRepository matchRepository;

    @Operation(summary = "Get all parent from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all parents",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Parent.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @CrossOrigin
    @GetMapping()
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<List<Parent>> getAllParents(){
        List<Parent> parents = parentRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(parents, status);
    }

    @Operation(summary = "Get a parent by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the parent",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Parent.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Parent not found",
                    content = @Content)})
    @CrossOrigin
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Parent> getParent(@PathVariable Long id){
        Parent returnParent = new Parent();
        HttpStatus status;

        if(parentRepository.findById(id).isPresent()){
            status = HttpStatus.OK;
            returnParent = parentRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnParent, status);
    }

    @Operation(summary = "Get players of given parent", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all players",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Player.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Parent not found",
                    content = @Content)
    })
    @CrossOrigin
    @GetMapping("/{id}/players")
    @PreAuthorize("hasRole('Admin') or hasRole('Parent') or hasRole('Coach')")
    public ResponseEntity<Set<Player>> getPlayersByParentId(@PathVariable Long id) {
        Set<Player> players = null;
        HttpStatus resp;
        if(parentRepository.findById(id).isPresent()){
            System.out.println("found parent by id: " +id);
            resp = HttpStatus.OK;
            players = parentRepository.findById(id).get().getPlayers();

            System.out.println(parentRepository.getOne(id).getUser().getFirstName());

            for(Player player : parentRepository.getOne(id).getPlayers()) {
                System.out.println("13123123" +player.getUser().getFirstName());
            }
        } else {
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(players, resp);
    }

    @Operation(summary = "Update a parent partially, by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the parent",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Parent not found",
                    content = @Content)
    })
    @CrossOrigin
    @PatchMapping("/{id}")
    @PreAuthorize("hasRole('Admin') or hasRole('Parent')")
    public ResponseEntity<HttpStatus> updateParentPartially(@PathVariable Long id, @RequestBody Parent parent) {

        Optional<Parent> parentOptional = parentRepository.findById(id);

        HttpStatus status;

        if(parentRepository.findById(id).isPresent()){
            Parent updParent = parentRepository.findById(id).get();
            if (!id.equals(updParent.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(status);
            }

            if(parent.getUser().getAddress() != null) {
                updParent.getUser().setAddress(parent.getUser().getAddress());
            }

            if(parent.getUser().getMobileNumber() > 0) {
                updParent.getUser().setMobileNumber(parent.getUser().getMobileNumber());
            }

            if(parent.getUser().getEmail() != null) {
                updParent.getUser().setEmail(parent.getUser().getEmail());
            }

            parentRepository.save(updParent);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Delete a parent by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the parent",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Parent.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Parent not found",
                    content = @Content)})
    @CrossOrigin
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public HttpStatus deleteParent(@PathVariable Long id){
        HttpStatus status;
        if(parentRepository.findById(id).isPresent()){
            Parent parent = parentRepository.findById(id).get();
            parentRepository.delete(parent);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

    @Operation(summary = "Set players to a given parent", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Parents added to the player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Parent.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Parent not found",
                    content = @Content)

    })
    @CrossOrigin
    @PatchMapping("/{id}/players")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Parent> setPlayers(@PathVariable Long id, @RequestBody ArrayList<Long> playerIds){
         HttpStatus status;
         if(parentRepository.findById(id).isPresent()){
             Parent parent = parentRepository.findById(id).get();
             for(Long player : playerIds){
                 if(playerRepository.findById(player).isPresent()){
                     Player playerToAdd = playerRepository.findById(player).get();
                     parent.players.add(playerToAdd);
                 }
             }
             status = HttpStatus.OK;
             parentRepository.save(parent);
             System.out.println(parent.players);
             return new ResponseEntity<>(parent, status);
         } else {
             status = HttpStatus.NOT_FOUND;
             return new ResponseEntity<>(status);
         }
    }

    @CrossOrigin
    @GetMapping("/{id}/children-matches")
    @Operation(summary = "Get either previous or upcoming matches for a parent", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "matches returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "matches for parent not found",
                    content = @Content)})
    @PreAuthorize("hasRole('Admin') or hasRole('Parent')")
    public ResponseEntity<List<Match>> getMatches(@PathVariable Long id,  @RequestParam String time){
        HttpStatus status;
        if(parentRepository.findById(id).isPresent()) {
            List<Match> matches;
            if(time.equalsIgnoreCase("previous")) {
                matches = matchRepository.getPreviousParentMatches(id);
                status = HttpStatus.OK;
                return new ResponseEntity<>(matches, status);
            }
            else if(time.equalsIgnoreCase("upcoming")) {
                matches = matchRepository.getUpComingParentMatches(id);
                status = HttpStatus.OK;
                return new ResponseEntity<>(matches, status);
            }
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

}

