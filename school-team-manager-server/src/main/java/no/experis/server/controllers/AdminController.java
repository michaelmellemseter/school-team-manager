package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.Admin;
import no.experis.server.repositories.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/admins")
public class AdminController {
    @Autowired
    private AdminRepository adminRepository;

    @Operation(summary = "Get all admins from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all admins",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Admin.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping()
    public ResponseEntity<List<Admin>> getAllAdmins(){
        List<Admin> admins = adminRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(admins, status);
    }

    @Operation(summary = "Get an admin by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the admin",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Admin.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Admin not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<Admin> getAdmin(@PathVariable Long id){
        Admin returnAdmin = new Admin();
        HttpStatus status;

        if(adminRepository.findById(id).isPresent()){
            status = HttpStatus.OK;
            returnAdmin = adminRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnAdmin, status);
    }


    @Operation(summary = "Update an admin by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the team",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Admin.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Admin not found",
                    content = @Content)
    })
    @CrossOrigin
    @PutMapping("/{id}")
    public ResponseEntity<Admin> updateAdmin(@PathVariable Long id, @RequestBody Admin admin){
        HttpStatus status;

        if(!id.equals(admin.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }

        if(adminRepository.findById(id).isPresent())
        {
            Admin returnAdmin = adminRepository.save(admin);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(returnAdmin, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }

    @Operation(summary = "Delete an admin by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the admin",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Admin.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Team not found",
                    content = @Content)})
    @CrossOrigin
    @DeleteMapping("/{id}")
    public HttpStatus deleteAdmin(@PathVariable Long id){
        HttpStatus status;
        if(adminRepository.findById(id).isPresent()){
            Admin admin = adminRepository.findById(id).get();
            adminRepository.delete(admin);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

}

