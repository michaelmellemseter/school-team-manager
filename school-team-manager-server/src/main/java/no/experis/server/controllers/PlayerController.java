package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.*;
import no.experis.server.repositories.MatchRepository;
import no.experis.server.repositories.ParentRepository;
import no.experis.server.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/v1/players")
public class PlayerController {
    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private ParentRepository parentRepository;

    @Autowired
    private MatchRepository matchRepository;

    @Operation(summary = "Get all player from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all players",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Player.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @CrossOrigin
    @GetMapping()
    @PreAuthorize("hasRole('Admin') or hasRole('Coach') or hasRole('Parent') or hasRole('Player')")
    public ResponseEntity<List<Player>> getAllPlayers(){
        List<Player> players = playerRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(players, status);
    }

    @Operation(summary = "Get a player by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)})
    @CrossOrigin
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin') or hasRole('Player')")
    public ResponseEntity<Player> getPlayer(@PathVariable Long id){
        Player returnPlayer = new Player();
        HttpStatus status;

        if(playerRepository.findById(id).isPresent()){
            status = HttpStatus.OK;
            returnPlayer = playerRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnPlayer, status);
    }

    @Operation(summary = "Update a player by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)
    })
    @CrossOrigin
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Player> updatePlayer(@PathVariable Long id, @RequestBody Player player){
        HttpStatus status;

        if(playerRepository.findById(id).isPresent()){
            Player returnPlayer = new Player();
            if(!id.equals(player.getId())){
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(returnPlayer, status);
            }
            returnPlayer = playerRepository.save(player);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(returnPlayer, status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Update a player partially, by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)
    })
    @CrossOrigin
    @PatchMapping("/{id}")
    @PreAuthorize("hasRole('Admin') or hasRole('Parent')")
    public ResponseEntity<HttpStatus> updatePlayerPartially(@PathVariable Long id, @RequestBody Player player) {
        HttpStatus status;

        if(playerRepository.findById(id).isPresent()){
            Player updPlayer = playerRepository.findById(id).get();

            if (!id.equals(updPlayer.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(status);
            }

            if(player.getUser().getAddress() != null) {
                updPlayer.getUser().setAddress(player.getUser().getAddress());
            }

            if(player.getUser().getMobileNumber() > 0) {
                updPlayer.getUser().setMobileNumber(player.getUser().getMobileNumber());
            }

            if(player.getUser().getEmail() != null) {
                updPlayer.getUser().setEmail(player.getUser().getEmail());
            }

            if(player.getMedicalNotes() != null) {
                updPlayer.setMedicalNotes(player.getMedicalNotes());
            }


            if(player.getProfilePicture() != null) {
                updPlayer.setProfilePicture(player.getProfilePicture());
            }

            if(player.getUser().getDate()!= null){
                updPlayer.getUser().setDate(player.getUser().getDate());
            }

            playerRepository.save(updPlayer);

            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);

        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Update a player's privacy, by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the player's privacy",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)
    })
    @CrossOrigin
    @PatchMapping("/privacy/{id}")
    @PreAuthorize("hasRole('Player')")
    public ResponseEntity<HttpStatus> updatePlayerPrivacy(@PathVariable Long id, @RequestBody Player player) {
        HttpStatus status;
        if(playerRepository.findById(id).isPresent())
        {
            Player updPlayer = playerRepository.findById(id).get();

            if (!id.equals(updPlayer.getId())) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(status);
            }

            updPlayer.setShowPersonalia(player.isShowPersonalia());
            updPlayer.setShowPhoto(player.isShowPhoto());
            updPlayer.setShowMedicalNotes(player.isShowMedicalNotes());

            playerRepository.save(updPlayer);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Delete a player by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)})
    @CrossOrigin
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<HttpStatus> deletePlayer(@PathVariable Long id){
        HttpStatus status;
        if(playerRepository.findById(id).isPresent()){
            Player player = playerRepository.findById(id).get();
            playerRepository.delete(player);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }



    @Operation(summary = "Set parents to a given player", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Parents added to the player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)

    })
    @CrossOrigin
    @PatchMapping("/{id}/parents")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Player> setParent(@PathVariable Long id, @RequestBody ArrayList<Long> parentIds){
        HttpStatus status;
        if(playerRepository.findById(id).isPresent()){
            Player player = playerRepository.findById(id).get();
            for(Long parent : parentIds){
                if(parentRepository.findById(parent).isPresent()){
                    Parent parentToAdd = parentRepository.findById(parent).get();
                    player.parents.add(parentToAdd);
                }
            }
            status = HttpStatus.NO_CONTENT;
            playerRepository.save(player);
            return new ResponseEntity<>(player, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }

    @CrossOrigin
    @GetMapping("/{id}/matches")
    @Operation(summary = "Get either previous or upcoming matches for a player", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "matches returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Match.class))}),
            @ApiResponse(responseCode = "400", description = "invalid request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)})
    @PreAuthorize("hasRole('Admin') or hasRole('Player')")
    public ResponseEntity<List<Match>> getMatches(@PathVariable Long id,  @RequestParam String time){
        HttpStatus status;
        if(playerRepository.findById(id).isPresent()) {
            List<Match> matches;
            if(time.equalsIgnoreCase("previous")) {
                matches = matchRepository.getPreviousPlayerMatches(id);
                status = HttpStatus.OK;
                return new ResponseEntity<>(matches, status);
            }
            else if(time.equalsIgnoreCase("upcoming")) {
                matches = matchRepository.getUpComingPlayerMatches(id);
                status = HttpStatus.OK;
                return new ResponseEntity<>(matches, status);
            }
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

    @Operation(summary = "Get teams for the given player", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all teams",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Team.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Player not found",
                    content = @Content)

    })
    @CrossOrigin
    @GetMapping("/{id}/teams")
    @PreAuthorize("hasRole('Admin') or hasRole('Player')")
    public ResponseEntity<Set<Team>> getTeamsByPlayerId(@PathVariable Long id) {

        HttpStatus status;
        if(playerRepository.findById(id).isPresent()){
            Set<Team> returnTeams = new HashSet<>();
            Player player = playerRepository.findById(id).get();
            Set<TeamPlayer> teams = player.getTeamPlayers();

            for(TeamPlayer team : teams){
                returnTeams.add(team.team);
            }
            status = HttpStatus.OK;
            return new ResponseEntity<>(returnTeams, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }

    }
}
