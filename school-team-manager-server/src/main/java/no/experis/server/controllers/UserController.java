package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.*;
import no.experis.server.models.dto.UserDTO;
import no.experis.server.repositories.*;
import no.experis.server.services.MailService;
import no.experis.server.services.PasswordService;
import no.experis.server.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private CoachRepository coachRepository;

    @Autowired
    private ParentRepository parentRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private NotificationRepository notificationRepository;


    //returns all users in the database
    @GetMapping
    @Operation(summary = "Get all users in the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all users",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = User.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)})
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> users = userRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(users, status);
    }

    //Returns a user object by Id. Checks if id exists in db and returns it.
    @Operation(summary = "Get a user by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "user not found",
                    content = @Content)})
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('Admin') or hasRole('Coach') or hasRole('Player') or hasRole('Parent')")
    public ResponseEntity<User> getUserById(@PathVariable Long id){
        User returnUser = new User();
        HttpStatus status;

        if(userRepository.findById(id).isPresent()){
            status = HttpStatus.OK;
            returnUser = userRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnUser, status);
    }

    @PostMapping()
    @Operation(summary = "Add a user to the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User added",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Role that needs to be added to user is not found",
                    content = @Content)})
    @PreAuthorize("hasRole('Admin') or hasRole('Coach')")
    public ResponseEntity<Map<String, Object>> addUser(@RequestBody UserDTO user) {

        if(roleRepository.findById(user.roleId).isPresent())
        {
            User dbUser = new User();
            Role role = roleRepository.findById(user.roleId).get();
            dbUser.setEmail(user.email);
            dbUser.setFirstName(user.firstName);
            dbUser.setSurName(user.surName);
            dbUser.setRole(role);
            userRepository.save(dbUser);
            Long roleDbId = userService.setUser(dbUser);
            String password = passwordService.getSaltString(); // Create a one-time password
            System.out.println(password);
            mailService.sendEmail(dbUser.getEmail(), "Your one-time password: " + password, "Account"); // Send a mail to the user with the one-time password
            HttpStatus resp = HttpStatus.CREATED;
            Map<String, Object> returnMap = new HashMap<>(); // Return the created user and the one-time password to the client
            returnMap.put("user", dbUser);
            returnMap.put("password", password);
            returnMap.put("roleDbId", roleDbId);
            return new ResponseEntity<>(returnMap, resp);
        } else {
            HttpStatus resp = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(resp);
        }
    }


    @Operation(summary = "Update a given User", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "user not found",
                    content = @Content)})
    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @RequestBody User user) {
        User returnUser = new User();
        HttpStatus status;
        //if it doesnt exist or pathvariable doesnt match id from object requested it returns a BAD_REQUEST.
        if (!id.equals(user.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnUser, status);
        }
        if(userRepository.findById(id).isPresent()){
            returnUser = userRepository.save(user);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnUser, status);
    }


    //Deletes a user recognized by id from the database.
    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    @Operation(summary = "Delete a user from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User deleted",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "user not found",
                    content = @Content)})
    @PreAuthorize("hasRole('Admin')")
    public HttpStatus deleteUser(@PathVariable Long id) {
        HttpStatus status;
        //Checks if it exists in db.
        if (userRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            userRepository.deleteById(id);
            System.out.println("User successfully deleted from database");
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

    //Get single player by passing user id.
    @GetMapping("/{id}/player")
    @Operation(summary = "Get player id for the given user", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Player found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "user not found",
                    content = @Content)})
    @PreAuthorize("hasRole('Player')")
    public ResponseEntity<Player> getPlayerByUserId(@PathVariable Long id) {
        Player returnPlayer = null;
        HttpStatus resp;
        if(userRepository.findById(id).isPresent()){
            System.out.println("found user by id: " +id);
            resp = HttpStatus.OK;
            for(Player player : playerRepository.findAll()){
                if(player.getUser().getId().equals(id)) {
                    returnPlayer = player;
                }
            }

        }else {
            System.out.println("not found");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnPlayer, resp);
    }

    @Operation(summary = "Get parent id for the given user", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Parent found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Parent.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "user not found",
                    content = @Content)})
    @GetMapping("/{id}/parent")
    @PreAuthorize("hasRole('Parent')")
    public ResponseEntity<Parent> getParentByUserId(@PathVariable Long id) {
        Parent returnParent = null;
        HttpStatus resp;
        if(userRepository.findById(id).isPresent()){
            System.out.println("found user by id: " +id);
            resp = HttpStatus.OK;
            for(Parent parent : parentRepository.findAll()){
                if(parent.getUser().getId().equals(id)) {
                    returnParent = parent;
                }
            }

        }else {
            System.out.println("not found");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnParent, resp);
    }

    @Operation(summary = "Get coach id for the given user", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Coach found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Coach.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "user not found",
                    content = @Content)})
    @GetMapping("/{id}/coach")
    @PreAuthorize("hasRole('Player') or hasRole('Coach')")
    public ResponseEntity<Coach> getCoachByUserId(@PathVariable Long id) {
        Coach returnCoach = null;
        HttpStatus resp;
        if(userRepository.findById(id).isPresent()){
            System.out.println("found user by id: " +id);
            resp = HttpStatus.OK;
            for(Coach coach : coachRepository.findAll()){
                if(coach.getUser().getId().equals(id)) {
                    returnCoach = coach;
                }
            }

        }else {
            System.out.println("not found");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCoach, resp);
    }

    @Operation(summary = "Get notifications for a user from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all notifications",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = Notification.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)
    })
    @CrossOrigin
    @GetMapping("/{id}/notifications")
    @PreAuthorize("hasRole('Parent')")
    public ResponseEntity<Set<Notification>>getNotifications(@PathVariable Long id){
        HttpStatus status;

        if(userRepository.findById(id).isPresent()){
            Set<Notification> notifications = notificationRepository.findAllByReceiverUserIdAndSeenIsFalse(id);
            status = HttpStatus.OK;
            return new ResponseEntity<>(notifications, status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }

}
