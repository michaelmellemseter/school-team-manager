package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.Location;
import no.experis.server.models.LocationType;
import no.experis.server.repositories.LocationTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "api/v1/locations/types")
public class LocationTypeController {

    @Autowired
    private LocationTypeRepository locationTypeRepository;

    @Operation(summary = "Get all location types from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all coaches",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = LocationType.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<LocationType>> getAllLocationTypes() {
        List<LocationType> locationTypes = locationTypeRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(locationTypes, resp);
    }

    @Operation(summary = "Get a location type by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the location type",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = LocationType.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location type not found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<LocationType> getLocationTypeById(@Parameter(description = "Id of location type to be searched") @PathVariable Long id) {
        LocationType returnLocationType = new LocationType();
        HttpStatus status;
        if (locationTypeRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            returnLocationType = locationTypeRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnLocationType, status);
    }

    @Operation(summary = "Update a location type by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the location type",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = LocationType.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
            content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location type not found",
                    content = @Content)
    })
    @CrossOrigin
    @PutMapping("/{id}")
    public ResponseEntity<LocationType> updateLocationType(@Parameter(description = "Id of location type to be updated")  @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The updated location type", required = true, content = @Content(
            schema = @Schema(implementation = LocationType.class))) @RequestBody LocationType locationType) {
        LocationType returnLocationType = new LocationType();
        HttpStatus status;
        if(!id.equals(locationType.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnLocationType, status);
        }
        if(locationTypeRepository.findById(id).isPresent())
        {
            returnLocationType = locationTypeRepository.save(locationType);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnLocationType, status);
    }

    @Operation(summary = "Create a location type", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new location type",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = LocationType.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
    })
    @PostMapping
    public ResponseEntity<LocationType> addLocationType(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The new location type", required = true, content = @Content(
            schema = @Schema(implementation = LocationType.class))) @RequestBody LocationType locationType) {
        LocationType returnLocationType = locationTypeRepository.save(locationType);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnLocationType, status);
    }

    @Operation(summary = "Delete a location type by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the location type",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = LocationType.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location type not found",
                    content = @Content)
    })
    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    public HttpStatus deleteLocationType(@Parameter(description = "Id of location type to be deleted") @PathVariable Long id) {
        HttpStatus status;
        if (locationTypeRepository.findById(id).isPresent()) {
            status= HttpStatus.OK;
            LocationType deleteLocationType = locationTypeRepository.findById(id).get();
            Set<Location> locations = deleteLocationType.locations;
            for (Location location : locations) location.locations.remove(deleteLocationType);
            locationTypeRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }
}
