package no.experis.server.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.experis.server.models.Location;
import no.experis.server.models.School;
import no.experis.server.repositories.SchoolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "api/v1/schools")
public class SchoolController {

    @Autowired
    private SchoolRepository schoolRepository;

    @Operation(summary = "Get all schools from the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all schools",
                    content = @Content(
                            mediaType = "application/json",
                            array =  @ArraySchema(
                                    schema = @Schema(implementation = School.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<School>> getAllSchools() {
        List<School> schools = schoolRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(schools, resp);
    }

    @Operation(summary = "Get a school by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the school",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = School.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "School not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<School> getSchoolById(@Parameter(description = "Id of school to be searched") @PathVariable Long id) {
        School returnSchool = new School();
        HttpStatus status;
        if (schoolRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            returnSchool = schoolRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnSchool, status);
    }

    @Operation(summary = "Update a school by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the school",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = School.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location not found",
                    content = @Content),
    })

    @CrossOrigin
    @PutMapping("/{id}")
    public ResponseEntity<School> updateSchool(@Parameter(description = "Id of school to be updated") @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The updated school", required = true, content = @Content(
            schema = @Schema(implementation = School.class))) @RequestBody School School) {
        School returnSchool = new School();
        HttpStatus status;
        if (!id.equals(School.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnSchool, status);
        }
        if(schoolRepository.findById(id).isPresent()){
            returnSchool = schoolRepository.save(School);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnSchool, status);
    }

    @Operation(summary = "Create a school", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new school",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = School.class))}),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity<School> addSchool(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The new school", required = true, content = @Content(
            schema = @Schema(implementation = School.class))) @RequestBody School School) {
        School returnSchool = schoolRepository.save(School);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnSchool, status);
    }

    @Operation(summary = "Delete a school by its id", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the school",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = School.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authenticated",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Location not found",
                    content = @Content)
    })
    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    public HttpStatus deleteSchool(@Parameter(description = "Id of school to be deleted") @PathVariable Long id) {
        HttpStatus status;
        if (schoolRepository.findById(id).isPresent()) {
            status = HttpStatus.OK;
            School deleteSchool = schoolRepository.findById(id).get();
            Set<Location> locations = deleteSchool.locations;
            for (Location location : locations) location.schools.remove(deleteSchool);
            schoolRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }
}

