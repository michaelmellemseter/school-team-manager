package no.experis.server.services;

import no.experis.server.models.*;
import no.experis.server.repositories.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class NotificationService {
    @Autowired
    NotificationRepository notificationRepository;
    public boolean teamParents(User senderUser, Team team, String message) {
        //boolean
        Set<Parent> uniqueParents = new HashSet<>();
        if(team.getTeamPlayers() != null){
            Set<TeamPlayer> teamPlayers = team.getTeamPlayers();

            for(TeamPlayer teamPlayer : teamPlayers){
                uniqueParents.addAll(teamPlayer.player.parents);
            }

            if(uniqueParents.isEmpty())
            {
                return false;
            }
            for (Parent parent : uniqueParents) {
                Notification notification = new Notification();
                notification.setMessage(message);
                notification.setSeen(false);
                notification.setSenderUser(senderUser);
                notification.setReceiverUser(parent.user);
                notificationRepository.save(notification);

            }
            return true;
        }
        return false;
    }
}
