package no.experis.server.services;

import no.experis.server.models.*;
import no.experis.server.repositories.AdminRepository;
import no.experis.server.repositories.CoachRepository;
import no.experis.server.repositories.ParentRepository;
import no.experis.server.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private CoachRepository coachRepository;
    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private PlayerRepository playerRepository;

    public Long setUser(User user) {
        switch(user.role.getRoleName()) {
            case "Admin" -> {
                Admin admin = new Admin();
                admin.setUser(user);
                adminRepository.save(admin);
                return admin.getId();
            }
            case "Coach" -> {
                Coach coach = new Coach();
                coach.setUser(user);
                coachRepository.save(coach);
                return coach.getId();
            }
            case "Parent" -> {
                Parent parent = new Parent();
                parent.setUser(user);
                parentRepository.save(parent);
                return parent.getId();
            }
            case "Player" -> {
                Player player = new Player();
                player.setShowPersonalia(false);
                player.setShowPhoto(false);
                player.setShowMedicalNotes(false);
                player.setUser(user);
                playerRepository.save(player);
                return player.getId();
            }
        }
        return -1L;
    }

}
