package no.experis.server.repositories;

import no.experis.server.models.MessageToAdmin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageToAdminRepository extends JpaRepository<MessageToAdmin, Long> {
    List<MessageToAdmin> findAllByApprovedIsNull();
}
