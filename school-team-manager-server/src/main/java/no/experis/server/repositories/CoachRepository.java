package no.experis.server.repositories;

import no.experis.server.models.Coach;
import no.experis.server.models.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CoachRepository extends JpaRepository<Coach, Long> {
}
