package no.experis.server.repositories;

import no.experis.server.models.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface MatchRepository extends JpaRepository<Match, Long> {
    List<Match> findByDateGreaterThanEqualOrderByDate(Date date);
    List<Match> findByHomeTeamIdInOrAwayTeamIdInAndDateGreaterThanEqualOrderByDate(Set<Long> homeIds, Set<Long> awayIds, Date date);
    List<Match> findByDateLessThanEqualAndCanceledReasonIsNullOrderByDateDesc(Date date);
    List<Match> findByHomeTeamIdInOrAwayTeamIdInAndDateLessThanEqualAndCanceledReasonIsNullOrderByDateDesc(Set<Long> homeIds, Set<Long> awayIds, Date date);

    /* Get matches for players */
    @Query(value =
            "SELECT m.* \n" +
            "FROM match m \n" +
            "JOIN team ON team.id=m.home_team_id OR team.id=m.away_team_id\n" +
            "JOIN team_player ON team.id=team_player.team_id\n" +
            "JOIN player ON team_player.player_id=player.id\n" +
            "WHERE player.id= :id\n" +
            "AND m.date > CURRENT_DATE", nativeQuery = true)
    List<Match> getUpComingPlayerMatches(Long id);

    @Query(value = "SELECT m.* \n" +
            "FROM match m \n" +
            "JOIN team ON team.id=m.home_team_id or team.id=m.away_team_id\n" +
            "JOIN team_player ON team.id=team_player.team_id\n" +
            "JOIN player ON team_player.player_id=player.id\n" +
            "WHERE player.id= :id\n" +
            "AND m.date < CURRENT_DATE", nativeQuery = true)
    List<Match> getPreviousPlayerMatches(Long id);

    /* Get matches for parents */
    @Query(value =
            "SELECT m.* \n" +
            "FROM match m \n" +
            "JOIN team ON team.id=m.home_team_id OR team.id=m.away_team_id\n" +
            "JOIN team_player ON team.id=team_player.team_id\n" +
            "JOIN player ON team_player.player_id=player.id\n" +
            "JOIN player_parent ON player.id = player_parent.player_id\n" +
            "WHERE player_parent.parent_id= :id\n" +
            "AND m.date > CURRENT_DATE", nativeQuery = true)
    List<Match> getUpComingParentMatches(Long id);

    @Query(value = "SELECT m.* \n" +
            "FROM match m \n" +
            "JOIN team ON team.id=m.home_team_id or team.id=m.away_team_id\n" +
            "JOIN team_player ON team.id=team_player.team_id\n" +
            "JOIN player ON team_player.player_id=player.id\n" +
            "JOIN player_parent ON player.id = player_parent.player_id\n" +
            "WHERE player_parent.parent_id= :id\n" +
            "AND m.date < CURRENT_DATE", nativeQuery = true)
    List<Match> getPreviousParentMatches(Long id);

    /* Get matches for coaches */
    @Query(value =
            "SELECT m.* \n" +
            "FROM match m \n" +
            "JOIN team ON team.id=m.home_team_id OR team.id=m.away_team_id\n" +
            "JOIN coach_team ON team.id=coach_team.team_id\n" +
            "WHERE coach_team.coach_id= :id\n" +
            "AND m.date > CURRENT_DATE", nativeQuery = true)
    List<Match> getUpComingCoachMatches(Long id);

    @Query(value = "SELECT m.* \n" +
            "FROM match m \n" +
            "JOIN team ON team.id=m.home_team_id or team.id=m.away_team_id\n" +
            "JOIN coach_team ON team.id=coach_team.team_id\n" +
            "WHERE coach_team.coach_id= :id\n" +
            "AND m.date < CURRENT_DATE", nativeQuery = true)
    List<Match> getPreviousCoachMatches(Long id);
}
