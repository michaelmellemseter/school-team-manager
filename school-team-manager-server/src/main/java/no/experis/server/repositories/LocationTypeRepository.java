package no.experis.server.repositories;

import no.experis.server.models.LocationType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationTypeRepository extends JpaRepository<LocationType, Long> {
}
