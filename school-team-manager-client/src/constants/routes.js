export const ROUTES = {
    LandingPage: '/',
    Home: '/home',
    Users: '/users',
    Coaches: '/coaches',
    Parents: '/parents',
    Players: '/players',
    Register: '/register',
    ProfileMain: '/profile',
    UpcomingMatches: '/matches',
    FinishedMatches: '/matches/finished',
    TeamsMain: '/teams',
    Schools: '/schools',
    Sports: '/sports',
    Locations: '/locations',
    LocationTypes: '/locations/types',
    MessagesToAdmin: '/admin/messages',
    SendMessageToAdmin: '/message',
    Notifications: '/notifications'
}
