export const ROLES = {
    Admin: 'Admin',
    Coach: 'Coach',
    Parent: 'Parent',
    Player: 'Player'
}