export default class RoleRepresentation {
    constructor(id, name, composite, clientRole, containerId) {
        this.id = id
        this.name = name
        this.composite = composite
        this.clientRole = clientRole
        this.containerId = containerId
    }
}