export default class User {
  constructor(roleId, email, firstName, surName) {
    this.roleId = roleId;
    this.email = email;
    this.firstName = firstName;
    this.surName = surName;
  }
}
