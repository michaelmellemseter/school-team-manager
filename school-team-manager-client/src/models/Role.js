export default class Role {
    constructor(id, roleName, keycloakId){
        this.id = id
        this.roleName = roleName
        this.keycloakId = keycloakId
    }
}