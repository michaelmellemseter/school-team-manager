export default class NotificationDTO {
  constructor(senderId, receiverId, message) {
    this.senderId = senderId;
    this.receiverId = receiverId;
    this.message = message;
  }
}
