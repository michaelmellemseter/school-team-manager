import { useState,useEffect } from "react"
import "../../css/list.css"
import AuthService from "../../services/AuthService"
import Modal from "react-modal";
import Loader from "../shared/loader/Loader";
import UserService from "../../services/UserService";
import NotificationService from "../../services/NotificationService";


function Notifications () {

    const [notifications, setNotifications] = useState([])
    const [notificationId, setNotificationId] = useState("")

    const [detailMessage, setDetailMessage] = useState("")
    const [detailUser, setDetailUser] = useState("")
    const [detailsShow, setDetailsShow] = useState(false)

    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        getNotifications()
        setLoading(false)
    }, []);

    const getNotifications = () => {
        UserService.getNotifications(AuthService.getDbId())
            .then((response) => {
                setNotifications(response.data)
            })
            .catch(error => console.error(error.notification))
    }

    const setDetails = (id, message, user) => {
        setNotificationId(id)
        setDetailMessage(message)
        setDetailUser(user)
    }

    const notificationSeen = () => {
        setDetailsShow(false)

        NotificationService.setNotificationSeen(notificationId)

        const updateState = notifications.filter(function(value, index, arr) {
            return value.id !== notificationId
        })
        setNotifications(updateState)
    }

    return (
        <div className="list-main">
            <div className="nav">
            </div>
            <Modal closeTimeoutMS={500} isOpen={detailsShow} onRequestClose={() => setDetailsShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            {detailMessage}
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={notificationSeen}>Seen</div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Notifications</h1>
            </div>
            <div className="list">
                {loading ? (
                    <div>
                        <Loader />
                    </div>
                ) : (
                    <ul>
                        {notifications.map((notification, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name" onClick={() => { setDetails(notification.id, notification.message, notification.senderUser) ; setDetailsShow(true) }}>
                                    New notification
                                </div>
                            </div>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    )
}

export default Notifications
