import Modal from "react-modal";
import "../../css/coaches.css";
import { useState, useEffect } from "react";
import UserService from "../../services/UserService";
import PlayerService from "../../services/PlayerService";
import NotifyModal from "../shared/notifymodal/NotifyModal";
import NotificationService from "../../services/NotificationService";
import placeholder from "../../assets/svg/placeholder.jpg";
import AuthService from "../../services/AuthService";
import ParentService from "../../services/ParentService";
import personalia from "../../assets/svg/personalia.svg";
import injury from "../../assets/svg/injury.svg";
import runner from "../../assets/svg/runner.svg";

function Players() {

  const [parents, setParents] = useState([]);
  const [players, setPlayers] = useState([]);
  const [updateModalShow, setUpdateModalShow] = useState(false);
  const [detailViewShow, setDetailViewShow] = useState(false);
  const [notifyModalShow, setNotifyModalShow] = useState(false);
  const [role, setRole] = useState("");
  const [playerParents, setPlayerParents] = useState([]);

  useEffect(() => {
    getRole();
    getAllPlayers();
    getAllParents();
  }, []);

  const getAllParents = () => {
    ParentService.getAllParents()
      .then((response) => {
        const allParents = response.data;
        setParents(allParents);
      })
      .catch((error) => console.error("Error, no kids"));
  };

  const getAllPlayers = () => {
    PlayerService.getAllPlayers()
      .then((response) => {
        const allPlayers = response.data;
        setPlayers(allPlayers);
      })
      .catch((error) => console.error("Error"));
  };

  const getRole = () => {
    const role = AuthService.getRole();
    setRole(role);
  };

  // delete player

  const deletePlayer = (id) => {
    UserService.deleteUser(id);

    const updateState = players.filter(function (value, index, arr) {
      return value.id !== id;
    });
    setPlayers(updateState);
  };

  //update existing player

  const [mobileNumber, setMobileNumber] = useState("");
  const [address, setAddress] = useState("");
  const [updateEmail, setUpdateEmail] = useState("");
  const [playerId, setPlayerId] = useState("");
  const [selectedPlayerId, setSelectedPlayerId] = useState("");

  const onChangeMobileNumber = (event) => {
    setMobileNumber(event.target.value);
  };
  const onChangeAddress = (event) => {
    setAddress(event.target.value);
  };
  const onChangeUpdateEmail = (event) => {
    setUpdateEmail(event.target.value);
  };

  const submitUpdate = () => {
    setUpdateModalShow(false);
    const updatePlayer = {
      id: playerId,
      user: {
        email: updateEmail === "" ? detailEmail : updateEmail,
        mobileNumber: mobileNumber === "" ? detailMobile : mobileNumber,
        address: address === "" ? detailAdress : address,
      },
    };
    PlayerService.updatePlayerPartially(playerId, updatePlayer);
    window.location.reload();
  };

  // detailview of player

  const [detailFirstName, setDetailFirstName] = useState("");
  const [detailSurName, setDetailSurName] = useState("");
  const [imgUrl, setImageUrl] = useState("");
  const [detailEmail, setDetailEmail] = useState("");
  const [detailMobile, setDetailMobile] = useState("");
  const [detailMedical, setDetailMedical] = useState("");
  const [detailDob, setDetailDob] = useState("");
  const [detailAdress, setDetailAdress] = useState("");
  const [showPersonalia, setShowPersonalia] = useState(false);
  const [showPicture, setShowPicture] = useState(false);
  const [showMedical, setShowMedical] = useState(false);

  // /(player.user.firstName, player.user.surName, player.profilePicture, player.user.email, player.user.mobileNumber,player.user.date, player.user.address , player.medicalNotes)
  const tempClickedUser = (
    fName,
    sName,
    img,
    detEmail,
    detMobile,
    detDob,
    detAdress,
    detMedical,
    detShowPersonal,
    detShowPicture,
    detShowMedical,
    detPlayerId,
    detParents
  ) => {
    setDetailFirstName(fName);
    setDetailSurName(sName);
    setImageUrl(img);
    setDetailEmail(detEmail);
    setDetailMobile(detMobile);
    setDetailMedical(detMedical);
    setDetailAdress(detAdress);
    setShowPersonalia(detShowPersonal);
    setShowMedical(detShowMedical);
    setShowPicture(detShowPicture);
    setSelectedPlayerId(detPlayerId);
    setPlayerParents(detParents);

    if (detDob !== null) {
      let dobFormat = detDob.split("T");
      let prettyFormat = dobFormat[0].toString();
      let prettyString = prettyFormat.split("-");
      let presentableFormat =
        prettyString[2] + "." + prettyString[1] + "." + prettyString[0];
      setDetailDob(presentableFormat);
    }
  };

  const [assignedParent, setAssignedParent] = useState("");

  const selectParent = (e) => {
    setAssignedParent(e.target.value);
  };

  const assignParent = () => {
    const parentIdInt = parseInt(assignedParent);
    const parent = [parentIdInt];
    PlayerService.addParentToPlayer(selectedPlayerId, parent);
  };

  const [feedbackShow, setFeedbackShow] = useState(false);
  const [feedback, setFeedback] = useState("");
  const [negativeFeedback, setNegativeFeedback] = useState(false);

  const userFeedBack = () => {
    if (
      playerParents.some((parent) => parent.id === parseInt(assignedParent))
    ) {
      setFeedback("Parent already assigned to this player");
      setNegativeFeedback(true);
    } else {
      setFeedback("Parent assigned");
      setNegativeFeedback(false);
      assignParent();
    }
    setFeedbackShow(true);
  };

  return (
    <div className="coaches-main">
      <div className="nav"></div>

      <Modal
        closeTimeoutMS={500}
        isOpen={updateModalShow}
        onRequestClose={() => setUpdateModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "0px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "0px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header-update">
            <h1 className="modal-title">
              Update {detailFirstName} {detailSurName}
            </h1>
          </div>

          <div className="modal-body">
            <div className="modal-form-update">
              <div className="update-input-div">
                <p>Mobile Number</p>
                <input
                  onChange={onChangeMobileNumber}
                  type="text"
                  placeholder={detailMobile}
                  className="modal-input-field-update"
                />
              </div>

              <div className="update-input-div">
                <p>Address</p>
                <input
                  onChange={onChangeAddress}
                  type="text"
                  placeholder={detailAdress}
                  className="modal-input-field-update"
                />
              </div>

              <div className="update-input-div">
                <p>Email</p>
                <input
                  onChange={onChangeUpdateEmail}
                  type="text"
                  placeholder={detailEmail}
                  className="modal-input-field-update"
                />
              </div>
            </div>
          </div>

          <div className="modal-footer">
            <div className="modal-add-btn-update" onClick={submitUpdate}>
              Update
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        closeTimeoutMS={500}
        isOpen={detailViewShow}
        onRequestClose={() => setDetailViewShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.10)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: " 0px solid var(--main-green) ",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "0px",
            outline: "none",
            padding: "0px",
          },
        }}
      >
        <div className="modal-main-container-detail">
          <div className="modal-detail-header-player">
            <h1>
              {detailFirstName} {detailSurName}{" "}
            </h1>
          </div>

          <div className="modal-container-detail">
            <div className="modal-left">
              {showPicture || role.includes("Admin") ? (
                <div className="image-container">
                  {imgUrl !== null ? (
                    <img id="image" src={imgUrl} alt="profile picture" />
                  ) : (
                    <img id="image" src={placeholder} alt="profile picture" />
                  )}
                </div>
              ) : (
                <div className="image-container-no">
                  <img id="image" src={placeholder} alt="profile picture" />
                  <p>Player has chosen to hide photo</p>
                </div>
              )}
              {role.includes("Coach") || role.includes("Admin") ? (
                <div className="select-parent">
                  <select
                    className="option"
                    onChange={(e) => selectParent(e)}
                    placeholder="assign parent"
                  >
                    <option
                      className="options"
                      selected="selected"
                      disabled="disabled"
                    >
                      Assign a parent
                    </option>
                    {parents.map((parent, index) => (
                      <option className="options" value={parent.id}>
                        {parent.user.firstName}
                      </option>
                    ))}
                  </select>
                  <div className="assign-button" onClick={() => userFeedBack()}>
                    Assign Parent
                  </div>
                </div>
              ) : (
                <div></div>
              )}
              {feedbackShow === false ? (
                <div></div>
              ) : negativeFeedback === true ? (
                <div className="user-feedback-false">{feedback}</div>
              ) : (
                <div className="user-feedback-true">{feedback}</div>
              )}
            </div>

            <div className="modal-right">
              {showPersonalia || role.includes("Admin") ? (
                <div className="modal-detail-body">
                  <div className="personalia-img">
                    <img src={personalia} />
                  </div>
                  <div class="personalia-detail-view">
                    <p>{detailEmail}</p>
                    <p>{detailMobile}</p>
                    <p>{detailDob}</p>
                    <p>{detailAdress}</p>
                  </div>
                </div>
              ) : (
                <div className="hidden-personalia">
                  Player has chosen to hide personal information
                </div>
              )}
              <div className="modal-detail-footer">
                <div className="injury-section-detail">
                  <div className="injury-header-detail">
                    <div className="injury-title">Injury Status</div>
                    <div className="injury-med">
                      <img width="25px" src={injury} />
                    </div>
                  </div>

                  <div className="injury-body">
                    {showMedical || role.includes("Admin") ? (
                      <p> {detailMedical} </p>
                    ) : (
                      <p>Player has chosen to hide injury status</p>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>

      <NotifyModal
        showing={notifyModalShow}
        closeFunction={setNotifyModalShow}
        receiverId={playerId}
        sendFunction={NotificationService.postNotificationPlayer}
      />

      <div className="title">
        <h1>Players</h1>
        <img className="runner" width="50px" src={runner} />
      </div>
      <div className="coaches-list">
        <ul>
          {players.map((player, index) => (
            <div className="coach-element" key={index}>
              <div
                className="coach-name"
                onClick={() => {
                  tempClickedUser(
                    player.user.firstName,
                    player.user.surName,
                    player.profilePicture,
                    player.user.email,
                    player.user.mobileNumber,
                    player.user.date,
                    player.user.address,
                    player.medicalNotes,
                    player.showPersonalia,
                    player.showPhoto,
                    player.showMedicalNotes,
                    player.id,
                    player.parents
                  );
                  setFeedbackShow(false);
                  setDetailViewShow(true);
                }}
              >
                {player.user.firstName} {player.user.surName}
              </div>
              {role.includes("Admin") || role.includes("Coach") ? (
                <div className="delete-update">
                  {role.includes("Admin") && (
                    <div
                      onClick={() => {
                        setPlayerId(player.id);
                        setUpdateModalShow(true);
                        tempClickedUser(
                          player.user.firstName,
                          player.user.surName,
                          player.profilePicture,
                          player.user.email,
                          player.user.mobileNumber,
                          player.user.date,
                          player.user.address,
                          player.medicalNotes,
                          player.showPersonalia,
                          player.showPhoto,
                          player.showMedicalNotes,
                          player.id,
                          player.parents
                        );
                      }}
                      className="update"
                    >
                      Update{" "}
                    </div>
                  )}
                  {role.includes("Admin") && (
                    <div
                      onClick={() => deletePlayer(player.user.id)}
                      className="delete"
                    >
                      Delete
                    </div>
                  )}
                  {role.includes("Coach") && (
                    <div
                      onClick={() => {
                        setPlayerId(player.user.id);
                        setNotifyModalShow(true);
                      }}
                      className="update"
                    >
                      Notify parents
                    </div>
                  )}
                </div>
              ) : (
                <div className="delete-update-hide"></div>
              )}
            </div>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Players;
