import "../../css/list.css"
import MatchService from "../../services/MatchService";
import {useEffect, useState} from "react";
import Modal from "react-modal";
import AuthService from "../../services/AuthService";
import PlayerService from "../../services/PlayerService";
import Loader from "../shared/loader/Loader";
import CoachService from "../../services/CoachService";
import ParentService from "../../services/ParentService";
import {MATCH_TIME} from "../../constants/matchTime";
import scoreicon from "../../assets/svg/scoreicon.svg"

function FinishedMatches() {

    const role = AuthService.getRole()
    const [matches, setMatches] = useState([])
    const [userMatches, setUserMatches] = useState([])
    const [matchId, setMatchId] = useState([])
    const [homeScore, setHomeScore] = useState([])
    const [awayScore, setAwayScore] = useState([])

    const [modalShow, setModalShow] = useState(false)
    const [showUserMatches, setShowUserMatches] = useState(false)

    const [loading, setLoading] = useState(false)


    useEffect(() => {
        setLoading(true)
        getAllFinishedMatches()
        setLoading(false)
    }, [])

    const getAllFinishedMatches = async () => {
        MatchService.getAllFinishedMatches([])
            .then((response) => {
                setMatches(response.data)
            })
            .catch(error => console.error(error.message))

        const roleDbId = AuthService.getRoleDbId();
        let service; // This service will be set according to the role of the logged in user
        switch (role) {
            case "Coach":
                service = CoachService;
                break;
            case "Player":
                service = PlayerService;
                break;
            case "Parent":
                service = ParentService;
                break;
            default:
                service = null;
                break;
        }

        const fetchMatches = async (service) => {
            const previousMatches = await service.getMatches(
                roleDbId,
                MATCH_TIME.Previous
            )
            setUserMatches(previousMatches.data);
        };

        if (service) fetchMatches(service);
    }

    const onChangeUserMatches = event => {
        setShowUserMatches(event.target.checked)
    }

    const setResult = () => {
        setModalShow(false)
        const updateMatch = {
            "id": matchId,
            "homeScore": homeScore,
            "awayScore": awayScore
        }
        MatchService.setScore(updateMatch.id, updateMatch)

        let updateState = matches
        for (let match in updateState) {
            if (updateState[match].id === matchId) {
                updateState[match].homeScore = homeScore
                updateState[match].awayScore = awayScore
                break
            }
        }
        setMatches(updateState)
    }

    const onChangeHomeScore = event => {
        setHomeScore(event.target.value)
    }

    const onChangeAwayScore = event => {
        setAwayScore(event.target.value)
    }

    return(
        <div className="list-main">
            <div className="nav">
            </div>
            <Modal isOpen={modalShow} onRequestClose={() => setModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Set result</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeHomeScore} type="text" placeholder="Home score" className="modal-input-field"/>
                            <input onChange={onChangeAwayScore} type="text" placeholder="Away score" className="modal-input-field"/>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={setResult}>Set result</div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Matches</h1>
                <img src={scoreicon} width="50px" className="runner"/>
            </div>
            {role.includes("Admin") ?
                <div className ="hide-btn">
                </div>
                :
                <div className="checkbox-field">
                    <input className="checkbox" type="checkbox" onChange={onChangeUserMatches}/>
                    <label className="checkbox-text">Only view your matches</label>
                </div>
            }
            <div className="list">
                {showUserMatches ? (
                    <ul>
                        {userMatches.map((match, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name">
                                    {match.homeTeam.name} vs {match.awayTeam.name}
                                    {match.homeScore >= 0 && match.awayScore >= 0 &&
                                    <div className="list-score">
                                        , Result: {match.homeScore} - {match.awayScore}
                                    </div>
                                    }
                                </div>
                                {(role.includes("Admin") || role.includes("Coach")) ?
                                    <div className="delete-update">
                                        <div onClick={() => {setMatchId(match.id); setModalShow(true); }} className="update">Set result</div>
                                    </div>
                                    :
                                    <div className ="hide-btn">
                                    </div>
                                }
                            </div>
                        ))}
                    </ul>
                ) : (
                    <ul>
                        {matches.map((match, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name">
                                    {match.homeTeam.name} vs {match.awayTeam.name}
                                    {match.homeScore >= 0 && match.awayScore >= 0 &&
                                    <div className="list-score">
                                        , Result: {match.homeScore} - {match.awayScore}
                                    </div>
                                    }
                                </div>
                                {(role.includes("Admin") || role.includes("Coach")) ?
                                    <div className="delete-update">
                                        <div onClick={() => {setMatchId(match.id); setModalShow(true); }} className="update">Set result</div>
                                    </div>
                                    :
                                    <div className ="hide-btn">
                                    </div>
                                }
                            </div>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    )
}

export default FinishedMatches
