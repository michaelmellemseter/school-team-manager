import { useState, useEffect } from "react";
import "../../css/list.css";
import Add from "@material-ui/icons/Add";
import Modal from "react-modal";
import AuthService from "../../services/AuthService";
import TeamService from "../../services/TeamService";
import SportService from "../../services/SportService";
import SchoolService from "../../services/SchoolService";
import LocationService from "../../services/LocationService";
import Loader from "../shared/loader/Loader";
import teamslogo from "../../assets/svg/teamslogo.svg";
import NotifyModal from "../shared/notifymodal/NotifyModal";
import NotificationService from "../../services/NotificationService";

function Teams() {

  const role = AuthService.getRole()
  const [teams, setTeams] = useState([]);
  const [schools, setSchools] = useState([]);
  const [sports, setSports] = useState([]);
  const [locations, setLocations] = useState([]);

  const [teamId, setTeamId] = useState("");
  const [name, setName] = useState("");
  const [school, setSchool] = useState("");
  const [sport, setSport] = useState("");
  const [location, setLocation] = useState("");

  const [updateName, setUpdateName] = useState("");
  const [updateSchool, setUpdateSchool] = useState("");
  const [updateSport, setUpdateSport] = useState("");
  const [updateLocation, setUpdateLocation] = useState("");

  const [modalShow, setModalShow] = useState(false);
  const [updateModalShow, setUpdateModalShow] = useState(false);
  const [deleteModalShow, setDeleteModalShow] = useState(false);
  const [notifyModalShow, setNotifyModalShow] = useState(false);

  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getAllTeams();
    getAllSchools();
    getAllSports();
    getAllLocations();
    setLoading(false);
  }, []);

  const getAllTeams = () => {
    TeamService.getAllTeams()
      .then((response) => {
        setTeams(response.data);
      })
      .catch((error) => console.error(error.message));
  };

  const getAllSchools = () => {
    SchoolService.getAllSchools()
      .then((response) => {
        setSchools(response.data);
      })
      .catch((error) => console.error(error.message));
  };

  const getAllSports = () => {
    SportService.getAllSports()
      .then((response) => {
        setSports(response.data);
      })
      .catch((error) => console.error(error.message));
  };

  const getAllLocations = () => {
    LocationService.getAllLocations()
      .then((response) => {
        setLocations(response.data);
      })
      .catch((error) => console.error(error.message));
  };

  const deleteTeam = (id) => {
    setDeleteModalShow(false);
    TeamService.deleteTeam(id);

    const updateState = teams.filter(function (value, index, arr) {
      return value.id !== id;
    });
    setTeams(updateState);
  };

  const onChangeName = (event) => {
    setName(event.target.value);
  };

  const onChangeSchool = (event) => {
    setSchool(event.target.value);
  };

  const onChangeSport = (event) => {
    setSport(event.target.value);
  };

  const onChangeLocation = (event) => {
    setLocation(event.target.value);
  };

  const newTeamForm = () => {
    if (
      name.length === 0 ||
      school === "-1" ||
      school.length === 0 ||
      sport === "-1" ||
      sport.length === 0 ||
      location === "-1" ||
      location.length === 0
    ) {
      setErrorMessage("You need to fill all the fields");
    } else {
      setModalShow(false);

      const newTeam = {
        name: name,
        school: { id: school },
        sport: { id: sport },
        location: { id: location },
      };
      TeamService.addTeam(newTeam);

      const updateState = teams;
      updateState.push(newTeam);
      setTeams(updateState);
    }
  };

  const onChangeUpdateName = (event) => {
    setUpdateName(event.target.value);
  };

  const onChangeUpdateSchool = (event) => {
    setUpdateSchool(event.target.value);
  };

  const onChangeUpdateSport = (event) => {
    setUpdateSport(event.target.value);
  };

  const onChangeUpdateLocation = (event) => {
    setUpdateLocation(event.target.value);
  };

  const setUpdateDetails = (id, name) => {
    setTeamId(id);
    setUpdateName(name);
  };

  const submitUpdate = () => {
    if (
      updateName.length === 0 ||
      updateSchool === "-1" ||
      updateSchool.length === 0 ||
      updateSport === "-1" ||
      updateSport.length === 0 ||
      updateLocation === "-1" ||
      updateLocation.length === 0
    ) {
      setErrorMessage("You need to fill all the fields");
    } else {
      setUpdateModalShow(false);

      const updateTeam = {
        id: teamId,
        name: updateName,
        school: { id: updateSchool },
        sport: { id: updateSport },
        location: { id: updateLocation },
      };
      TeamService.updateTeam(teamId, updateTeam);

      let updateState = teams;
      for (let team in updateState) {
        if (updateState[team].id === teamId) {
          updateState[team] = updateTeam;
          break;
        }
      }
      setTeams(updateState);
    }
  };

  return (
    <div className="list-main">
      <NotifyModal
        showing={notifyModalShow}
        closeFunction={setNotifyModalShow}
        receiverId={teamId}
        sendFunction={NotificationService.postNotificationTeam}
      />
      <Modal
        closeTimeoutMS={500}
        isOpen={modalShow}
        onRequestClose={() => setModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Add New Team</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <input
                onChange={onChangeName}
                type="text"
                placeholder="Name"
                className="modal-input-field"
              />
              <label htmlFor="school">School*:</label>
              <select
                onChange={onChangeSchool}
                name="school"
                id="school"
                className="modal-select"
              >
                <option value="-1">Choose school...</option>
                {schools.map((school, index) => (
                  <option value={school.id}>{school.name}</option>
                ))}
              </select>
              <label htmlFor="sport">Sport*:</label>
              <select
                onChange={onChangeSport}
                name="sport"
                id="sport"
                className="modal-select"
              >
                <option value="-1">Choose sport...</option>
                {sports.map((sport, index) => (
                  <option value={sport.id}>{sport.name}</option>
                ))}
              </select>
              <label htmlFor="location">Location:</label>
              <select
                onChange={onChangeLocation}
                name="location"
                id="location"
                className="modal-select"
              >
                <option value="-1">Choose location...</option>
                {locations.map((location, index) => (
                  <option value={location.id}>{location.name}</option>
                ))}
              </select>
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={newTeamForm}>
              Add Team
            </div>
          </div>
          <div className="modal-error-message">{errorMessage}</div>
        </div>
      </Modal>
      <Modal
        closeTimeoutMS={500}
        isOpen={updateModalShow}
        onRequestClose={() => setUpdateModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Update Team</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <input
                onChange={onChangeUpdateName}
                type="text"
                placeholder={updateName}
                className="modal-input-field"
              />
              <label htmlFor="school">School:</label>
              <select
                onChange={onChangeUpdateSchool}
                name="school"
                id="school"
                className="modal-select"
              >
                <option value="-1">Choose school...</option>
                {schools.map((school, index) => (
                  <option value={school.id}>{school.name}</option>
                ))}
              </select>
              <label htmlFor="sport">Sport:</label>
              <select
                onChange={onChangeUpdateSport}
                name="sport"
                id="sport"
                className="modal-select"
              >
                <option value="-1">Choose sport...</option>
                {sports.map((sport, index) => (
                  <option value={sport.id}>{sport.name}</option>
                ))}
              </select>
              <label htmlFor="location">Location:</label>
              <select
                onChange={onChangeUpdateLocation}
                name="location"
                id="location"
                className="modal-select"
              >
                <option value="-1">Choose location...</option>
                {locations.map((location, index) => (
                  <option value={location.id}>{location.name}</option>
                ))}
              </select>
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={submitUpdate}>
              Update Team
            </div>
          </div>
          <div className="modal-error-message">{errorMessage}</div>
        </div>
      </Modal>
      <Modal
        closeTimeoutMS={500}
        isOpen={deleteModalShow}
        onRequestClose={() => setDeleteModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Delete Team</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <p className="team-p">
                Are you sure you want to delete this team?
              </p>
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={() => deleteTeam(teamId)}>
              Delete Team
            </div>
          </div>
        </div>
      </Modal>
      <div className="title">
        <h1>Teams</h1>
        <img width="70px" className="runner" src={teamslogo} />
      </div>
      <div className="list">
        {loading ? (
          <div>
            <Loader />
          </div>
        ) : (
          <ul>
            {role.includes("Admin") ? (
              <div className="list-add-btn" onClick={() => setModalShow(true)}>
                <Add fontSize="large" />{" "}
                <p className="add-new-text">Add New Team</p>
              </div>
            ) : (
              <div className="hide-add-btn"></div>
            )}
            {teams.map((team, index) => (
              <div className="list-element" key={index}>
                <div className="list-name">{team.name}</div>
                {role.includes("Admin") || role.includes("Coach") ? (
                  <div className="delete-update">
                    {role.includes("Admin") && (
                      <div
                        onClick={() => {
                          setUpdateDetails(team.id, team.name);
                          setUpdateModalShow(true);
                        }}
                        className="update"
                      >
                        Update
                      </div>
                    )}
                    {role.includes("Admin") && (
                      <div
                        onClick={() => {
                          setTeamId(team.id);
                          setDeleteModalShow(true);
                        }}
                        className="delete"
                      >
                        Delete
                      </div>
                    )}
                    {(role.includes("Admin") || role.includes("Coach")) && (
                      <div
                        onClick={() => {
                          setTeamId(team.id);
                          setNotifyModalShow(true);
                        }}
                        className="update"
                      >
                        Notify parents
                      </div>
                    )}
                  </div>
                ) : (
                  <div className="delete-update-hide"></div>
                )}
              </div>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
}

export default Teams;
