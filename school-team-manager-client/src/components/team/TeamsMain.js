import AuthService from "../../services/AuthService"
import { useState } from "react"
import { useEffect } from "react"
import Teams from "../../components/team/Teams"
import YourTeams from "../../components/team/YourTeams"
import PlayerTeams from "../../components/team/PlayerTeams";

function ProfileMain() {

    useEffect(() => {
        getRole();
      }, []);

    const [role, setRole] = useState("")

    const getRole = () => {
        const role = AuthService.getRole();
        setRole(role);
    }

    return(
        <div>
            {role.includes("Coach") ? 
             <YourTeams/>
            : role.includes("Player")?
            <PlayerTeams/>
            :
            <Teams/>
            }
        </div>
    )
}

export default ProfileMain
