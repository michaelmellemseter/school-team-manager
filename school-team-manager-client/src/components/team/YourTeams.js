import Modal from "react-modal"
import {useState, useEffect} from "react"
import UserService from "../../services/UserService"
import AuthService from "../../services/AuthService"
import PlayerService from "../../services/PlayerService"
import TeamService from "../../services/TeamService";
import "../../css/coaches.css"
import "../../css/yourTeams.css"
import addplayer from "../../assets/svg/addplayer.svg"
import searchtwo from "../../assets/svg/searchcolor.svg"
import teamslogo from "../../assets/svg/teamslogo.svg"
import SchoolService from "../../services/SchoolService";
import SportService from "../../services/SportService";
import LocationService from "../../services/LocationService";
import Add from "@material-ui/icons/Add";


function YourTeams() {

    const userId = AuthService.getDbId();
    const role = AuthService.getRole()

    const [teamId, setTeamId] = useState(""); 
    const [user, setUser] = useState("");
    const [coach, setCoach] = useState("");
    const [players, setPlayers] = useState([])
    const [allPlayers, setAllPlayers] = useState([])
    const [schools, setSchools] = useState([])
    const [sports, setSports] = useState([])
    const [locations, setLocations] = useState([])

    const [modalShow, setModalShow] = useState(false)
    const [addModalShow, setAddModalShow] = useState(false)
    const [updateModalShow, setUpdateModalShow] = useState(false)
    const [deleteModalShow, setDeleteModalShow] = useState(false)

    const [firstName, setFirstName] = useState("");
    const [surName, setSurName] = useState(""); 
    const [email, setEmail] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [date, setDate] = useState(""); 
    const [address, setAddress] = useState(""); 
    const [teams, setTeams] = useState([]);
    const [teamName, setTeamName] = useState("");

    const [name, setName] = useState("")
    const [school, setSchool] = useState("")
    const [sport, setSport] = useState("")
    const [location, setLocation] = useState("")

    const [updateName, setUpdateName] = useState("")
    const [updateSchool, setUpdateSchool] = useState("")
    const [updateSport, setUpdateSport] = useState("")
    const [updateLocation, setUpdateLocation] = useState("")

    const [search, setSearch] = useState(""); 
    const [searchResult, setSearchResult] = useState([]);

    const [errorMessage, setErrorMessage] = useState("")
    
    useEffect(()=> {
        getUserById(userId)
        getCoachByUserId(userId)
        getAllPlayers();
        getAllSchools()
        getAllSports()
        getAllLocations()
    },[])

    useEffect(()=> {
        setSearchResult(
        allPlayers.filter(player =>
            (player.user.firstName.toLowerCase() + " " + 
            player.user.surName.toLowerCase())
            .includes(search.toLowerCase()))
        )
    }, [search])

    const getAllPlayers = () => {
        PlayerService.getAllPlayers()
        .then((response) => {
            const allPlayers = response.data;
            setAllPlayers(allPlayers)
        })
        .catch(error => console.error("Error"))
    }

    const getAllSchools = () => {
        SchoolService.getAllSchools()
            .then((response) => {
                setSchools(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const getAllSports = () => {
        SportService.getAllSports()
            .then((response) => {
                setSports(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const getAllLocations = () => {
        LocationService.getAllLocations()
            .then((response) => {
                setLocations(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const getUserById = (id) => {
        UserService.getUserById(id)
        .then((response) => {
            const user = response.data; 
            setUser(user);
            setFirstName(user.firstName)
            setSurName(user.surName)
            setEmail(user.email)
            setMobileNumber(user.mobileNumber)
            setAddress(user.address)
            setDate(user.date)
        }).catch(error=>console.error("error, no user"))

    }

    const getCoachByUserId = (id) => {
        UserService.getCoachByUserId(id)
        .then((response) => {
            const coach = response.data
            setCoach(coach)
            setTeams(coach.teams)
        })
    }

    const getPlayersByTeamId = (id) => {
        TeamService.getPlayersByTeamId(id)
        .then((response) => {
            const players = response.data
            setPlayers(players) 
        })
    }

    const tempTeam = (name, index, teamId) => {
        getPlayersByTeamId(coach.teams[index].id)
        setTeamName(name)
        setTeamId(teamId)
    }

    const onChangeSearch = event => {
        setSearch(event.target.value); 
    }

    const [jerseyNumber, setJerseyNumber] = useState("")
    const [confirmModal, setConfirmModal] = useState(false)

    const [currentPlayerFirstName, setCurrentPlayerFirstName] = useState("")
    const [currentPlayerSurName, setCurrentPlayerSurName] = useState("")
    const [currentPlayerId, setCurrentPlayerId] = useState("")

    const addPlayerToTeam = (playerId,playerFirstname, playerSurname) => {
        setConfirmModal(true)
        setCurrentPlayerFirstName(playerFirstname)
        setCurrentPlayerId(playerId)
        setCurrentPlayerSurName(playerSurname)
    }   

    const jerseyNumberInput = event => {
        setJerseyNumber(event.target.value); 
    }
    
    const addPlayerToTeamConfirm = () => {
        const playerWithJersey = {
            "playerId" : currentPlayerId,
            "jerseyNr" : jerseyNumber
        }

        const player = {
            "player": {
            "user" : {
                "firstName" : currentPlayerFirstName,
                "surName" : currentPlayerSurName
            } 
        }
        }
        TeamService.addPlayerToTeam(teamId, playerWithJersey)
        const updateState = players
        updateState.push(player)
        setPlayers(updateState)
    }

    const deletePlayerFromTeam = (playerId) => {
        TeamService.deletePlayerFromTeam(teamId, playerId)
    }

    const onChangeName = event => {
        setName(event.target.value)
    }

    const onChangeSchool = event => {
        setSchool(event.target.value)
    }

    const onChangeSport = event => {
        setSport(event.target.value)
    }

    const onChangeLocation = event => {
        setLocation(event.target.value)
    }

    const onChangeUpdateName = event => {
        setUpdateName(event.target.value)
    }

    const newTeamForm = async () => {
        if (name.length === 0 || school === '-1' || school.length === 0 || sport === '-1' || sport.length === 0 || location === '-1' || location.length === 0) {
            setErrorMessage("You need to fill all the fields")
        }
        else {
            setAddModalShow(false)

            const newTeam =
                {
                    "name": name,
                    "school": {"id": school},
                    "sport": {"id": sport},
                    "location": {"id": location}
                }

            let returnedTeam = null
            await TeamService.addTeam(newTeam)
                .then((response) => {
                    returnedTeam = response.data
                })

            const arrayCoachId = [AuthService.getRoleDbId()]

            TeamService.assignCoachToTeam(returnedTeam.id, arrayCoachId)

            const updateState = teams
            updateState.push(newTeam)
            setTeams(updateState)
        }
    }

    const onChangeUpdateSchool = event => {
        setUpdateSchool(event.target.value)
    }

    const onChangeUpdateSport = event => {
        setUpdateSport(event.target.value)
    }

    const onChangeUpdateLocation = event => {
        setUpdateLocation(event.target.value)
    }

    const setUpdateDetails = (id, name) => {
        setTeamId(id)
        setUpdateName(name)
    }

    const submitUpdate = () => {
        if (updateName.length === 0 || updateSchool === '-1' || updateSchool.length === 0 || updateSport === '-1' || updateSport.length === 0 || updateLocation === '-1' || updateLocation.length === 0) {
            setErrorMessage("You need to fill all the fields")
        } else {
            setUpdateModalShow(false)

            const updateTeam =
                {
                    "id": teamId,
                    "name": updateName,
                    "school": {"id": updateSchool},
                    "sport": {"id": updateSport},
                    "location": {"id": updateLocation}
                }
            TeamService.updateTeam(teamId, updateTeam)

            let updateState = teams
            for (let team in updateState) {
                if (updateState[team].id === teamId) {
                    updateState[team] = updateTeam
                    break
                }
            }
            setTeams(updateState)
        }
    }

    const deleteTeam = (id) => {
        setDeleteModalShow(false)
        TeamService.deleteTeam(id)

        const updateState = teams.filter(function(value, index, arr) {
            return value.id !== id
        })
        setTeams(updateState)
    }

    return (
        <div className="coaches-main">
            <div className="nav">
            </div>
            <Modal closeTimeoutMS={500} isOpen={modalShow} onRequestClose={() => setModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.10)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '200px',
                           right: '200px',
                           bottom: '100px',
                           border: '0px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '0px'
                       }
                   }}>
                <div className="modal-yourteams-container">
                    <div className="modal-child-header">
                        <h1> {teamName} </h1>
                    </div>
                    <div className="modal-yourteams-body">
                        <div className="yourteams-left">
                            <div className="yourteam-players-title">
                                <h2>Players in team</h2>
                            </div>
                            {players.map((player, index) => (
                                <div className="player-element-teams">
                                    <div className="modal-teams-player" key={index}> {player.player.user.firstName} {player.player.user.surName}</div>

                                    <div className="jerseyNr-mapped"> {player.jerseyNr} </div>
                                    <div onClick={()=> deletePlayerFromTeam(player.player.id)} className="delete-teams">
                                        delete
                                    </div>
                                </div>
                            ))
                            }
                        </div>
                        <div>
                        </div>
                        <div className="yourteams-right">
                            <div className="add-player-to-team">
                                <div>
                                    <h2>Add player to team</h2>
                                </div>
                                <div className="add-image-container">
                                    <img src={addplayer} width="30px"/>
                                </div>
                            </div>
                            <div className="search-player">
                                <img width="20px" src={searchtwo}/>
                                <input className="search-box" type="text" placeholder="Search for player" onChange={onChangeSearch} />
                            </div>
                            <div className="search-result">
                                {searchResult.map((player, index) => (
                                    <div className="add-player-team">
                                        <div key={index} className="modal-teams-player"> {player.user.firstName} {player.user.surName}</div>
                                        <div onClick={() => addPlayerToTeam(player.id,player.user.firstName, player.user.surName)} className="add-teams"> Add </div>
                                    </div>
                                ))
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={confirmModal} onRequestClose={() => setConfirmModal(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.10)'
                       },
                       content: {
                           position: 'absolute',
                           top: '300px',
                           left: '500px',
                           right: '500px',
                           bottom: '300px',
                           border: '0px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '0px'
                       }
                   }}>
                <div className="modal-jersey-container">
                    <div className="modal-jersey-header">
                        <h3> Enter Jersey number </h3>
                    </div>
                    <div className="input-jersey">
                        <input className="input-jerseyNr" placeholder="#NR" onChange={jerseyNumberInput} type="number" />
                    </div>
                    <div className="modal-jersey-footer">
                        <div className="add-jerseynr" onClick={() => addPlayerToTeamConfirm()}>
                            Add number
                        </div>
                    </div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={addModalShow} onRequestClose={() => setAddModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Add New Team</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeName} type="text" placeholder="Name" className="modal-input-field"/>
                            <label htmlFor="school">School*:</label>
                            <select onChange={onChangeSchool} name="school" id="school" className="modal-select">
                                <option value="-1">Choose school...</option>
                                {schools.map((school, index) => (
                                    <option value={school.id}>{school.name}</option>
                                ))}
                            </select>
                            <label htmlFor="sport">Sport*:</label>
                            <select onChange={onChangeSport} name="sport" id="sport" className="modal-select">
                                <option value="-1">Choose sport...</option>
                                {sports.map((sport, index) => (
                                    <option value={sport.id}>{sport.name}</option>
                                ))}
                            </select>
                            <label htmlFor="location">Location:</label>
                            <select onChange={onChangeLocation} name="location" id="location" className="modal-select">
                                <option value="-1">Choose location...</option>
                                {locations.map((location, index) => (
                                    <option value={location.id}>{location.name}</option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={newTeamForm}>Add Team</div>
                    </div>
                    <div className="modal-error-message">{errorMessage}</div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={updateModalShow} onRequestClose={() => setUpdateModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Update Team</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeUpdateName} type="text" placeholder={updateName} className="modal-input-field"/>
                            <label htmlFor="school">School:</label>
                            <select onChange={onChangeUpdateSchool} name="school" id="school" className="modal-select">
                                <option value="-1">Choose school...</option>
                                {schools.map((school, index) => (
                                    <option value={school.id}>{school.name}</option>
                                ))}
                            </select>
                            <label htmlFor="sport">Sport:</label>
                            <select onChange={onChangeUpdateSport} name="sport" id="sport" className="modal-select">
                                <option value="-1">Choose sport...</option>
                                {sports.map((sport, index) => (
                                    <option value={sport.id}>{sport.name}</option>
                                ))}
                            </select>
                            <label htmlFor="location">Location:</label>
                            <select onChange={onChangeUpdateLocation} name="location" id="location" className="modal-select">
                                <option value="-1">Choose location...</option>
                                {locations.map((location, index) => (
                                    <option value={location.id}>{location.name}</option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={submitUpdate}>Update Team</div>
                    </div>
                    <div className="modal-error-message">{errorMessage}</div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={deleteModalShow} onRequestClose={() => setDeleteModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Delete Team</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <p className="team-p">Are you sure you want to delete this team?</p>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={ () => deleteTeam(teamId)}>Delete Team</div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Your Teams</h1>
                <img width="70px" className="runner" src={teamslogo} />
            </div>
            <div className="coaches-list">
                <ul>
                    {role.includes("Coach") ?
                        <div className="list-add-btn" onClick={() => setAddModalShow(true)}>
                            <Add fontSize="large"/> <p className="add-new-text">Add New Team</p>
                        </div>
                        :
                        <div className ="hide-add-btn">
                        </div>
                    }
                    {teams.map((team, index) => (
                        <div className="coach-element">
                            <div onClick={ () => {setModalShow(true); tempTeam(team.name, index, team.id) }} className="coach-name" key={index}> {team.name}</div>
                            {role.includes("Coach") ?
                                <div className="delete-update">
                                    <div onClick={() => {setUpdateDetails(team.id, team.name) ; setUpdateModalShow(true); }} className="update">Update</div>
                                    <div onClick={ () => {setTeamId(team.id) ; setDeleteModalShow(true)}} className="delete">Delete</div>
                                </div>
                                :
                                <div className="delete-update-hide">
                                </div>
                            }
                        </div>
                    ))
                    }
                </ul>
            </div>
        </div>
    )
}

export default YourTeams;
