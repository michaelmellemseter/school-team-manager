import "../../css/list.css"
import MatchService from "../../services/MatchService";
import {useEffect, useState} from "react";
import TeamService from "../../services/TeamService";
import Add from "@material-ui/icons/Add";
import Modal from "react-modal";
import LocationService from "../../services/LocationService";
import AuthService from "../../services/AuthService";
import PlayerService from "../../services/PlayerService";
import Loader from "../shared/loader/Loader";
import CoachService from "../../services/CoachService";
import ParentService from "../../services/ParentService";
import {MATCH_TIME} from "../../constants/matchTime";
import matchicon from "../../assets/svg/matchicon.svg"

function UpcomingMatches() {

    const role = AuthService.getRole()
    const [matches, setMatches] = useState([])
    const [userMatches, setUserMatches] = useState([])
    const [teams, setTeams] = useState([])
    const [locations, setLocations] = useState([])

    const [matchId, setMatchId] = useState("")
    const [homeTeam, setHomeTeam] = useState("")
    const [awayTeam, setAwayTeam] = useState("")
    const [location, setLocation] = useState("")
    const [date, setDate] = useState("")
    const [time, setTime] = useState("")
    const [canceledReason, setCanceledReason] = useState("")

    const [detailHomeTeam, setDetailHomeTeam] = useState("")
    const [detailAwayTeam, setDetailAwayTeam] = useState("")
    const [detailLocation, setDetailLocation] = useState("")
    const [detailDate, setDetailDate] = useState("")
    const [detailTime, setDetailTime] = useState("")
    const [detailCanceled, setDetailCanceled] = useState("")

    const [modalShow, setModalShow] = useState(false)
    const [detailMatchShow, setDetailMatchShow] = useState(false)
    const [modalCancel, setModalCancel] = useState(false)
    const [showUserMatches, setShowUserMatches] = useState(false)

    const [errorMessage, setErrorMessage] = useState("")
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        setLoading(true)
        getAllUpcomingMatches()
        getAllTeams()
        getAllLocations()
        setLoading(false)
    }, [])

    const getAllUpcomingMatches = async () => {
        MatchService.getAllUpcomingMatches([])
            .then((response) => {
                setMatches(response.data)
            })
            .catch(error => console.error(error.message))

        const roleDbId = AuthService.getRoleDbId();
        let service; // This service will be set according to the role of the logged in user
        switch (role) {
            case "Coach":
                service = CoachService;
                break;
            case "Player":
                service = PlayerService;
                break;
            case "Parent":
                service = ParentService;
                break;
            default:
                service = null;
                break;
        }

        const fetchMatches = async (service) => {
            const upcomingMatches = await service.getMatches(
                roleDbId,
                MATCH_TIME.Upcoming
            )
            setUserMatches(upcomingMatches.data);
        };

        if (service) fetchMatches(service);
    }

    const getAllTeams = () => {
        TeamService.getAllTeams()
            .then((response) => {
                setTeams(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const getAllLocations = () => {
        LocationService.getAllLocations()
            .then((response) => {
                setLocations(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const newMatchForm = () => {
        if (homeTeam === '-1' || homeTeam.length === 0 || awayTeam === '-1' || awayTeam.length === 0 || location === '-1' || location.length === 0) {
            setErrorMessage("You need to fill all the fields")
        } else {
            setModalShow(false)
            const newMatch =
                {
                    "date": date,
                    "time": time,
                    "homeTeam": {"id": homeTeam},
                    "awayTeam": {"id": awayTeam},
                    "location": {"id": location}
                }
            MatchService.addMatch(newMatch)

            window.location.reload(false)
        }
    }

    const cancelMatch = () => {
        setModalCancel(false)
        const updateMatch = {
            "id": matchId,
            "canceledReason": canceledReason
        }
        MatchService.setReason(matchId, updateMatch)

        let updateState = matches
        for (let match in updateState) {
            if (updateState[match].id === matchId) {
                updateState[match].canceledReason = canceledReason
                break
            }
        }
        setMatches(updateState)
    }

    const setMatchDetails = (homeTeam, awayTeam, location, date, time, canceledReason) => {
        setDetailHomeTeam(homeTeam)
        setDetailAwayTeam(awayTeam)
        setDetailLocation(location)
        setDetailDate(date)
        setDetailTime(time)
        setDetailCanceled(canceledReason)
    }

    const onChangeHomeTeam = event => {
        setHomeTeam(event.target.value)
    }

    const onChangeAwayTeam = event => {
        setAwayTeam(event.target.value)
    }

    const onChangeLocation = event => {
        setLocation(event.target.value)
    }

    const onChangeDate = event => {
        setDate(event.target.value)
    }

    const onChangeTime = event => {
        setTime(event.target.value)
    }

    const onChangeReason = event => {
        setCanceledReason(event.target.value)
    }

    const onChangeUserMatches = event => {
        setShowUserMatches(event.target.checked)
    }

    return(
        <div className="list-main">
            <div className="nav">
            </div>
            <Modal isOpen={modalShow} onRequestClose={() => setModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">New match</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <label className="modal-label" htmlFor="homeTeam">Home team:</label>
                            <select onChange={onChangeHomeTeam} name="homeTeam" id="homeTeam" className="modal-select">
                                <option value="-1">Choose home team...</option>
                                {teams.map((team, index) => (
                                    <option value={team.id}>{team.name}</option>
                                ))}
                            </select>
                            <label htmlFor="awayTeam">Away team:</label>
                            <select onChange={onChangeAwayTeam} name="awayTeam" id="awayTeam" className="modal-select">
                                <option value="-1">Choose away team...</option>
                                {teams.map((team, index) => (
                                    <option value={team.id}>{team.name}</option>
                                ))}
                            </select>
                            <label htmlFor="locations">Location:</label>
                            <select onChange={onChangeLocation} name="locations" id="locations" className="modal-select">
                                <option value="-1">Choose location...</option>
                                {locations.map((location, index) => (
                                    <option value={location.id}>{location.name}</option>
                                ))}
                            </select>
                            <input onChange={onChangeDate} className="modal-select" type="date"/>
                            <input onChange={onChangeTime} className="modal-select" type="time"/>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={newMatchForm}>Create match</div>
                    </div>
                    <div className="modal-error-message">{errorMessage}</div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={detailMatchShow} onRequestClose={() => setDetailMatchShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">{detailHomeTeam.name} vs {detailAwayTeam.name}</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <p>Location: {detailLocation.name}</p>
                            <br/>
                            <p>Date: {detailDate.slice(0, 10)}</p>
                            <br/>
                            <p>Time: {detailTime.slice(0, 5)}</p>
                            <br/>
                            {detailCanceled !== null &&
                            <p>Reason of cancellation: {detailCanceled}</p>
                            }
                        </div>
                    </div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={modalCancel} onRequestClose={() => setModalCancel(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Cancel Match</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeReason} type="text" placeholder="Reason" className="modal-input-field"/>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={cancelMatch}>Cancel match</div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Matches</h1>
                <img className="runner" width="60px" src={matchicon} />
            </div>
            {role.includes("Admin") ?
                <div className ="hide-btn">
                </div>
                :
                <div className="checkbox-field">
                    <input className="checkbox" type="checkbox" onChange={onChangeUserMatches}/>
                    <label className="checkbox-text">Only view your matches</label>
                </div>
            }
            <div className="list">
                {showUserMatches ? (
                    <ul>
                        {(role.includes("Admin") || role.includes("Coach")) &&
                            <div className="list-add-btn" onClick={() => setModalShow(true)}>
                                <Add fontSize="large"/> <p className="add-new-text">Add match</p>
                            </div>
                        }
                        {userMatches.map((match, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name" onClick={() => { setMatchDetails(match.homeTeam, match.awayTeam, match.location, match.date, match.time, match.canceledReason) ; setDetailMatchShow(true) }}>
                                    {match.homeTeam.name} vs {match.awayTeam.name}
                                    {match.canceledReason !== null &&
                                    <div className="canceled">
                                        CANCELED
                                    </div>
                                    }
                                </div>
                                {(role.includes("Admin") || role.includes("Coach")) ?
                                    <div className="delete-update">
                                        <div onClick={() => {setMatchId(match.id) ; setModalCancel(true)}} className="delete">Cancel</div>
                                    </div>
                                    :
                                    <div className ="hide-btn">
                                    </div>
                                }
                            </div>
                        ))}
                    </ul>
                ) : (
                    <ul>
                        {(role.includes("Admin") || role.includes("Coach")) &&
                        <div className="list-add-btn" onClick={() => setModalShow(true)}>
                            <Add fontSize="large"/> <p className="add-new-text">Add match</p>
                        </div>
                        }
                        {matches.map((match, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name" onClick={() => { setMatchDetails(match.homeTeam, match.awayTeam, match.location, match.date, match.time, match.canceledReason) ; setDetailMatchShow(true) }}>
                                    {match.homeTeam.name} vs {match.awayTeam.name}
                                    {match.canceledReason !== null &&
                                    <div className="canceled">
                                        CANCELED
                                    </div>
                                    }
                                </div>
                                {(role.includes("Admin") || role.includes("Coach")) ?
                                    <div className="delete-update">
                                        <div onClick={() => {setMatchId(match.id) ; setModalCancel(true)}} className="delete">Cancel</div>
                                    </div>
                                    :
                                    <div className ="hide-btn">
                                    </div>
                                }
                            </div>
                        ))}
                    </ul>
                )}

            </div>
        </div>
    )
}

export default UpcomingMatches
