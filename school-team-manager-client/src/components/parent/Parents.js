import Modal from "react-modal";
import "../../css/coaches.css"
import "../../css/updateModal.css"
import { useState,useEffect } from "react"
import ParentService from "../../services/ParentService";
import UserService from "../../services/UserService"
import AuthService from "../../services/AuthService"
import parenticonsmall from "../../assets/svg/parenticon.svg"
import personalia from "../../assets/svg/personalia.svg"
import runner from "../../assets/svg/runner.svg"


function Parents() {

    const [players, setPlayers] = useState([])
    const [parents, setParents] = useState([])
    const [updateModalShow, setUpdateModalShow] = useState(false)
    const [detailViewShow, setDetailViewShow] = useState(false)
    const [role, setRole] = useState(""); 


    useEffect(() => {
        getRole();
        getAllParents();
    }, []);

    const getAllParents = () => {
        ParentService.getAllParents()
            .then((response) => {
                const allParents = response.data;
                setParents(allParents)
            })
            .catch(error => console.error("Error, no kids"))
    }

    const getRole = () => {
        const role = AuthService.getRole();
        setRole(role);

    }

    const getAllChildren = (parent) => {
        ParentService.getAllChildren(parent)
            .then((response) => {
                const allPlayers = response.data;
                setPlayers(allPlayers)
            })
            .catch(error => console.error("Error"))
    }

    // delete parent

    const deleteParent = (id) => {
        UserService.deleteUser(id)

        const updateState = parents.filter(function(value, index, arr) {
            return value.id !== id
        })
        setParents(updateState)
    }

    
     //update existing parent

    const [mobileNumber, setMobileNumber] = useState("");
    const [address, setAddress] = useState("");
    const [updateEmail, setUpdateEmail] = useState("");
    const [parentId, setParentId] = useState("");

    
    const onChangeMobileNumber = event => {
        setMobileNumber(event.target.value)
    }
    const onChangeAddress = event => {
        setAddress(event.target.value)
    }
    const onChangeUpdateEmail = event => {
        setUpdateEmail(event.target.value)
    }

    const submitUpdate = () => {
        setUpdateModalShow(false)
        const updateParent = {
            
            "id": parentId,
            "user": {
                "email": updateEmail ==="" ? detailEmail : updateEmail,
                "mobileNumber": mobileNumber ==="" ? detailMobile : mobileNumber,
                "address": address ==="" ? detailAdress : address,
            }
        }
        ParentService.updateParent(parentId,updateParent)
        window.location.reload();
    }


    // detailview of parent

    const [detailFirstName, setDetailFirstName] = useState("")
    const [detailSurName , setDetailSurName] = useState("")
    const [detailEmail, setDetailEmail] = useState("");
    const [detailMobile, setDetailMobile] = useState("");
    const [detailAdress, setDetailAdress ] = useState("");
    const [detailDob, setDetailDob] = useState("");


    // /(player.user.firstName, player.user.surName, player.profilePicture, player.user.email, player.user.mobileNumber,player.user.date, player.user.address , player.medicalNotes)
    const tempClickedUser = (fName, sName, detEmail,detMobile, detDob, detAdress, detParentId) => {

        getAllChildren(detParentId)

        setDetailFirstName(fName)
        setDetailSurName(sName)

        setDetailEmail(detEmail)
        setDetailMobile(detMobile)

        setDetailAdress(detAdress)

        if(detDob !== null) {
        let dobFormat = detDob.split("T")
        let prettyFormat = dobFormat[0].toString()
        let prettyString = prettyFormat.split("-")
        let presentableFormat = prettyString[2] + "." + prettyString[1] + "." + prettyString[0]
        setDetailDob(presentableFormat)
        }

    }

    return(
        <div className="coaches-main">
            <Modal closeTimeoutMS={500} isOpen={updateModalShow} onRequestClose={() => setUpdateModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '0px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header-update">
                        <h1 className="modal-title">Update {detailFirstName} {detailSurName} </h1>
                    </div>

                    <div className="modal-body">
                        <div className="modal-form-update">

                            <div className="update-input-div">
                                <p>Mobile Number</p>
                                <input onChange={onChangeMobileNumber} type="text" placeholder={detailMobile} className="modal-input-field-update"/>
                            </div>

                            <div className="update-input-div">
                                <p>Address</p>
                                <input onChange={onChangeAddress} type="text" placeholder={detailAdress} className="modal-input-field-update"/>
                            </div>

                            <div className="update-input-div">
                                <p>Email</p>
                                <input onChange={onChangeUpdateEmail} type="text" placeholder={detailEmail} className="modal-input-field-update"/>
                            </div>

                        </div>
                    </div>

                    <div className="modal-footer">
                        <div className="modal-add-btn-update" onClick={submitUpdate} >Update</div>
                    </div>

                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={detailViewShow} onRequestClose={() => setDetailViewShow(false)}

                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.10)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '0px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '0px'
                       }
                   }}>
                <div className="modal-container-detail-parent">
                    <div className="modal-detail-header-player">
                        <h1> {detailFirstName} {detailSurName} </h1>
                    </div>
                    <div className="modal-body-parent">
                        <div className="modal-detail-parent">
                            <div className="personalia-parent">
                                <h2>Personalia</h2><img src={personalia} />
                            </div>
                            <div className="personalia-parent-detail-view">
                                <div className="detail-label">
                                    <div className="info">
                                        <p>Email:</p> <p>{detailEmail}</p>
                                    </div>
                                    {detailMobile !== null?
                                        <div className="info">
                                            <p>Mobile:</p> <p>{detailMobile}</p>
                                        </div>
                                        :
                                        <div className="info">
                                            <p>Mobile:</p> <p>No number et</p>
                                        </div>
                                    }
                                    {detailAdress !== null?
                                        <div className="info">
                                            <p>Address:</p> <p>{detailAdress}</p>
                                        </div>
                                        :
                                        <div className="info">
                                            <p>Address:</p> <p>No address set</p>
                                        </div>
                                    }
                                    {detailDob !== "" ?
                                        <div className="info">
                                            <p>Date of Birth:</p> <p>{detailDob}</p>
                                        </div>
                                        :
                                        <div className="info">
                                            <p>Date of Birth:</p> <p>No DOB set</p>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="modal-detail-children">
                            <div className="children-title-div">
                                <h2 className="children-title">Children</h2>
                                <img  width="40px" src={runner} />
                            </div>
                            {players.map((player,index) =>
                                <div className="children-parent">
                                    {player.user.firstName}  {player.user.surName}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Parents</h1>
                <img className="runner" width="30px" src={parenticonsmall}/>
            </div>
            <div className="coaches-list">
                <ul>
                    {parents.map((parent, index) => (
                        <div className="coach-element" key={index}>
                            <div className="coach-name" onClick= {() =>{tempClickedUser(parent.user.firstName, parent.user.surName, parent.user.email, parent.user.mobileNumber,parent.user.date, parent.user.address, parent.id)  ; setDetailViewShow(true)}}>
                                {parent.user.firstName} {parent.user.surName}
                            </div>
                            {role.includes("Admin")?
                                <div className="delete-update">
                                    <div onClick={() => {setParentId(parent.id); tempClickedUser(parent.user.firstName, parent.user.surName, parent.user.email, parent.user.mobileNumber,parent.user.date, parent.user.address, parent.id) ; setUpdateModalShow(true); }} className="update">Update </div>
                                    <div onClick={ () => deleteParent(parent.user.id)} className="delete">Delete</div>
                                </div>
                                :
                                <div className="delete-update-hide">
                                </div>
                            }
                        </div>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default Parents;
