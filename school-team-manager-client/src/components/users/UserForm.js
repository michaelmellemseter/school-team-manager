import HttpService from "../../services/HttpService";
import UserService from "../../services/UserService";
import { useState, useEffect } from "react";
import LoadingButton from "../shared/LoadingButton";
import "./users.css";
import Role from "../../models/Role";
import User from "../../models/User";
import AuthService from "../../services/AuthService";

function UserForm(props) {
  const [yourRole, setYourRole] = useState(AuthService.getRole());
  const [roles, setRoles] = useState([]);
  const axiosClient = HttpService.getAxiosClient();
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState(0);
  const [message, setMessage] = useState(false);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadingRoles, setLoadingRoles] = useState(false);

  useEffect(() => {
      setLoadingRoles(true);
      getRoles();
      setLoadingRoles(false);
  }, []);

    const getRoles = async () => {
        if (yourRole === "Admin") {
            axiosClient(`${process.env.REACT_APP_API_BASE_URL}/roles`)
                .then((res) => {
                    setRoles(res.data);
                    setRole(res.data[0].id);
                })
                .catch((err) => console.error(err));
        } else {
            await axiosClient(`${process.env.REACT_APP_API_BASE_URL}/roles`)
                .then((res) => {
                    const updateState = res.data.filter(function (value, index, arr) {
                        return value.roleName !== "Admin" && value.roleName !== "Coach";
                    });
                    setRoles(updateState);
                    setRole(updateState[0].id);
                })
                .catch((err) => console.error(err));
        }
    };

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        setError(false);
        setMessage(false);
        setLoading(true);

        const chosenRole = roles.find((obj) => obj.id == role); // Get the role obj based on id

        // Create role to post to DB
        const roleObj = new Role(
            chosenRole.id,
            chosenRole.roleName,
            chosenRole.keycloakId
        );
        // Create the user to post to DB
        const user = new User(roleObj.id, email, name, surname);

        try {
            await UserService.postUser(user, roleObj);
            setMessage("Successfully created the user!");
        } catch (err) {
            setError(err);
        } finally {
            setLoading(false);
        }
    };

    return (
        <form className="user-form" onSubmit={handleSubmit}>
            <label for="name-input">Name</label>
            <input
                id="name-input"
                className="form-input"
                type="text"
                onChange={(evt) => setName(evt.target.value)}
                required
            />
            <br />
            <br />
            <label for="email-input">Surname</label>
            <input
                id="surname-input"
                className="form-input"
                type="text"
                onChange={(evt) => setSurname(evt.target.value)}
                required
            />
            <br />
            <br />
            <label for="email-input">Email</label>
            <input
                id="email-input"
                className="form-input"
                type="email"
                onChange={(evt) => setEmail(evt.target.value)}
                required
            />
            <br />
            <br />
            <label for="select-input">Role</label>
            <select
                id="select-input"
                className="form-select"
                onChange={(evt) => setRole(evt.currentTarget.value)}
                disabled={loadingRoles}
            >
                {roles.map((role) => (
                    <option key={role.id} value={role.id}>
                        {role.roleName}
                    </option>
                ))}
            </select>
            <br />
            <br />

            <LoadingButton
                type="submit"
                loading={loading}
                message={message}
                error={error}
            >
                Add user
            </LoadingButton>
        </form>
    );
}


export default UserForm;
