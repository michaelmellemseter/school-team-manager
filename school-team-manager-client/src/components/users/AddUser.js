import UserForm from "./UserForm";

function AddUser() {
  return (
    <div>
      <h1>Add a user</h1>
      <p>Add a user to the system by sending them an invite</p>
      <br />
      <UserForm />
      <br />
      <br />
    </div>
  );
}

export default AddUser;
