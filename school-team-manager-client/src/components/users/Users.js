import { useState, useEffect } from "react";
import AuthService from "../../services/AuthService";
import UserService from "../../services/UserService";
import AddUser from "./AddUser";
import { ROLES } from "../../constants/roles";
import "./users.css";
import Loader from "../shared/loader/Loader";
import UserItem from "./UserItem";

function Users() {
  const [users, setUsers] = useState([]);
  const role = AuthService.getRole();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getAllUsers();
  }, []);

  const getAllUsers = () => {
    setLoading(true);
    UserService.getAllUsers()
      .then((res) => {
        setUsers(res.data);
        setLoading(false);
      })
      .catch((error) => console.error("Error"));
  };

  return (
    <div className="users-main">
      <div className="users-box">
        <h1>Users</h1>
        {loading ? (
          <div>
            <p>Loading users...</p>
            <Loader />
          </div>
        ) : (
          <div className="users-list-container">
            <ul className="user-form-list">
              {users.map((user) => (
                <UserItem key={user.id} user={user} />
              ))}
            </ul>
          </div>
        )}
      </div>
      {(role === ROLES.Admin || role === ROLES.Coach) && <AddUser />}
    </div>
  );
}

export default Users;
