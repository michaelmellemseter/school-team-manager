import AuthService from "../../services/AuthService";
import AdminHome from "./AdminHome";
import OtherHome from "./OtherHome";
function Home() {
  const role = AuthService.getRole();

  if (role === "Admin") return <AdminHome />;
  else return <OtherHome role={role} />;
}

export default Home;
