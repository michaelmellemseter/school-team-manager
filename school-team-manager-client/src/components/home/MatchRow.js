import "./matchrow.css";

function MatchRow({ match }) {
  return (
    <div className="row-container">
      <div className="row-item">
        <p className="p-first">
          <span>{match.homeScore}</span> <span>{match.homeTeam.name}</span>
        </p>
        <p className="p-last">
          <span>{match.awayScore}</span> <span>{match.awayTeam.name}</span>
        </p>
      </div>
      <div className="row-item">
        <p>{match.date}</p>
      </div>
      <div className="row-item">
        <img
          src={`${process.env.PUBLIC_URL}/resources/${match.homeTeam.sport.name}.svg`}
          alt={`${match.homeTeam.sport.name}-icon`}
        />
      </div>
    </div>
  );
}

export default MatchRow;
