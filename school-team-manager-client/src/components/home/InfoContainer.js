import "./infocontainer.css";
import MatchRow from "./MatchRow";

function InfoContainer({ title, titleColor, matches }) {
  if (matches.length !== 0) {
    return (
      <div className="info-container">
        <div className={"title-container " + titleColor}>
          <h2>{title}</h2>
        </div>
        {matches.map((match) => (
          <MatchRow key={match.id} match={match} />
        ))}
      </div>
    );
  } else return <p>No {title}</p>;
}

export default InfoContainer;
