import "./home.css";
import Logo from "../../assets/run_icon.svg";

function AdminHome() {
  return (
    <div className="main-box">
      <h1 className="main-header">School Team Manager</h1>
      <img src={Logo} alt="STM logo" />
      <br />
      <br />
      <p>Use the navigation bar to administer the system</p>
    </div>
  );
}

export default AdminHome;
