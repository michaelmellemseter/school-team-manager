import { useState, useEffect } from "react";
import "../../css/list.css";
import Add from "@material-ui/icons/Add";
import Modal from "react-modal";
import AuthService from "../../services/AuthService";
import SchoolService from "../../services/SchoolService";
import LocationService from "../../services/LocationService";
import schoolicon from "../../assets/svg/schoolicon.svg";
import Loader from "../shared/loader/Loader";
import NotifyModal from "../shared/notifymodal/NotifyModal";
import NotificationService from "../../services/NotificationService";

function Schools() {

  const role = AuthService.getRole()
  const [schools, setSchools] = useState([]);
  const [locations, setLocations] = useState([]);

  const [schoolId, setSchoolId] = useState("");
  const [name, setName] = useState("");
  const [location, setLocation] = useState([]);

  const [updateName, setUpdateName] = useState("");
  const [updateLocation, setUpdateLocation] = useState([]);

  const [modalShow, setModalShow] = useState(false);
  const [updateModalShow, setUpdateModalShow] = useState(false);
  const [deleteModalShow, setDeleteModalShow] = useState(false);
  const [notifyModalShow, setNotifyModalShow] = useState(false);

  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getAllSchools();
    getAllLocations();
    setLoading(false);
  }, []);

  const getAllSchools = () => {
    SchoolService.getAllSchools()
      .then((response) => {
        setSchools(response.data);
      })
      .catch((error) => console.error(error.message));
  };

  const getAllLocations = () => {
    LocationService.getAllLocations()
      .then((response) => {
        setLocations(response.data);
      })
      .catch((error) => console.error(error.message));
  };

  const deleteSchool = (id) => {
    setDeleteModalShow(false);
    SchoolService.deleteSchool(id);

    const updateState = schools.filter(function (value, index, arr) {
      return value.id !== id;
    });
    setSchools(updateState);
  };

  const onChangeName = (event) => {
    setName(event.target.value);
  };

  const onChangeLocation = (event) => {
    let array = location;
    if (event.target.checked) {
      array.push(event.target.value);
      setLocation(array);
    } else {
      array = array.filter(function (value, index, arr) {
        return value !== event.target.value;
      });
      setLocation(array);
    }
  };

  const newSchoolForm = () => {
    if (name.length === 0) {
      setErrorMessage("You need to fill all the fields");
    } else {
      setModalShow(false);

      let loc = [];
      for (let l of location) {
        loc.push({ id: l });
      }

      const newSchool = {
        name: name,
        locations: loc,
      };
      SchoolService.addSchool(newSchool);

      const updateState = schools;
      updateState.push(newSchool);
      setSchools(updateState);
      setLocation([]);
    }
  };

  const onChangeUpdateName = (event) => {
    setUpdateName(event.target.value);
  };

  const onChangeUpdateLocation = (event) => {
    let array = updateLocation;
    if (event.target.checked) {
      array.push(event.target.value);
      setUpdateLocation(array);
    } else {
      array = array.filter(function (value, index, arr) {
        return value !== event.target.value;
      });
      setUpdateLocation(array);
    }
  };

  const setUpdateDetails = (id, name) => {
    setSchoolId(id);
    setUpdateName(name);
  };

  const submitUpdate = () => {
    if (updateName.length === 0) {
      setErrorMessage("You need to fill all the fields");
    } else {
      setUpdateModalShow(false);

      let loc = [];
      for (let l of updateLocation) {
        loc.push({ id: l });
      }

      const updateSchool = {
        id: schoolId,
        name: updateName,
        locations: loc,
      };
      SchoolService.updateSchool(schoolId, updateSchool);

      let updateState = schools;
      for (let school in updateState) {
        if (updateState[school].id === schoolId) {
          updateState[school] = updateSchool;
          break;
        }
      }
      setSchools(updateState);
      setUpdateLocation([]);
    }
  };

  return (
    <div className="list-main">
      <Modal
        closeTimeoutMS={500}
        isOpen={modalShow}
        onRequestClose={() => setModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Add New School</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <input
                onChange={onChangeName}
                type="text"
                placeholder="Name"
                className="modal-input-field"
              />
              <label className="modal-checkbox-title">Locations:</label>
              <div className="modal-checkbox-field">
                {locations.map((location, index) => (
                  <div>
                    <input
                      className="modal-checkbox"
                      type="checkbox"
                      onChange={onChangeLocation}
                      value={location.id}
                    />
                    <label className="modal-checkbox-text">
                      {location.name}
                    </label>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={newSchoolForm}>
              Add School
            </div>
          </div>
          <div className="modal-error-message">{errorMessage}</div>
        </div>
      </Modal>
      <Modal
        closeTimeoutMS={500}
        isOpen={updateModalShow}
        onRequestClose={() => setUpdateModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Update School</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <input
                onChange={onChangeUpdateName}
                type="text"
                placeholder={updateName}
                className="modal-input-field"
              />
              <label className="modal-checkbox-title">Locations:</label>
              <div className="modal-checkbox-field">
                {locations.map((location, index) => (
                  <div>
                    <input
                      className="modal-checkbox"
                      type="checkbox"
                      onChange={onChangeUpdateLocation}
                      value={location.id}
                    />
                    <label className="modal-checkbox-text">
                      {location.name}
                    </label>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={submitUpdate}>
              Update School
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        closeTimeoutMS={500}
        isOpen={deleteModalShow}
        onRequestClose={() => setDeleteModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Delete School</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <p className="team-p">
                Are you sure you want to delete this school?
              </p>
            </div>
          </div>
          <div className="modal-footer">
            <div
              className="modal-add-btn"
              onClick={() => deleteSchool(schoolId)}
            >
              Delete School
            </div>
          </div>
          <div className="modal-error-message">{errorMessage}</div>
        </div>
      </Modal>
      <NotifyModal
        showing={notifyModalShow}
        closeFunction={setNotifyModalShow}
        receiverId={schoolId}
        sendFunction={NotificationService.postNotificationSchool}
      />
      <div className="title">
        <h1>Schools</h1>
        <img
          width="40px"
          className="runner"
          src={schoolicon}
          style={{ marginBottom: 10 }}
        />
      </div>
      <div className="list">
        {loading ? (
          <div>
            <Loader />
          </div>
        ) : (
          <ul>
            {role.includes("Admin") ? (
              <div className="list-add-btn" onClick={() => setModalShow(true)}>
                <Add fontSize="large" />{" "}
                <p className="add-new-text">Add New School</p>
              </div>
            ) : (
              <div className="hide-add-btn"></div>
            )}
            {schools.map((school, index) => (
              <div className="list-element" key={index}>
                <div className="list-name">{school.name}</div>
                {role.includes("Admin") ? (
                  <div className="delete-update">
                    <div
                      onClick={() => {
                        setUpdateDetails(school.id, school.name);
                        setUpdateModalShow(true);
                      }}
                      className="update"
                    >
                      Update
                    </div>
                    <div
                      onClick={() => {
                        setSchoolId(school.id);
                        setDeleteModalShow(true);
                      }}
                      className="delete"
                    >
                      Delete
                    </div>
                    <div
                      onClick={() => {
                        setSchoolId(school.id);
                        setNotifyModalShow(true);
                      }}
                      className="update"
                    >
                      Notify parents
                    </div>
                  </div>
                ) : (
                  <div className="delete-update-hide"></div>
                )}
              </div>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
}

export default Schools;
