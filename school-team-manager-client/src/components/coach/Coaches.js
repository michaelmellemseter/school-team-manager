import { useState,useEffect } from "react"
import CoachService from "../../services/CoachService";
import UserService from "../../services/UserService";
import "../../css/coaches.css"
import Modal from "react-modal";
import AuthService from "../../services/AuthService"
import TeamService from "../../services/TeamService"
import coachicon from "../../assets/svg/coachicon.svg"
import teamslogo from "../../assets/svg/teamslogo.svg"


function Coaches () {

    const [coaches, setCoach] = useState([])
    const [updateModalShow, setUpdateModalShow] = useState(false)
    const [assignTeamsModalShow, setAssignTeamsModalShow] = useState(false)
    const [role, setRole] = useState(""); 
    const [teams, setTeams] = useState([])
    const [assignedTeam, setAssignedTeam] = useState("")

    useEffect(() => {
        getRole();
        getAllCoaches();
        getAllTeams();
    }, []);

    const getAllCoaches = () => {
        CoachService.getAllCoaches()
        .then((response) => {
            const allCoaches = response.data;
            setCoach(allCoaches)
        })
        .catch(error => console.error("Error"))
    }

    const getAllTeams = () => {
        TeamService.getAllTeams()
        .then((response) => {
            const allTeams = response.data;
            setTeams(allTeams)
        })
        .catch(error => console.error("Error"))
    }

    const deleteCoach = (id) => {
        UserService.deleteUser(id)
    }

    const getRole = () => {
        const role = AuthService.getRole();
        setRole(role);
    }

    //update existing coach
    const [mobileNumber, setMobileNumber] = useState("");
    const [address, setAddress] = useState("");
    const [updateEmail, setUpdateEmail] = useState("");
    const [coachId, setCoachId] = useState("");
    
    const onChangeMobileNumber = event => {
        setMobileNumber(event.target.value)
    }
    const onChangeAddress = event => {
        setAddress(event.target.value)
    }
    const onChangeUpdateEmail = event => {
        setUpdateEmail(event.target.value)
    }

    const submitUpdate = () => {
        setUpdateModalShow(false)
        const updateCoach =
            {
                "id": coachId,
                "user": {
                    "email": updateEmail ==="" ? detailEmail : updateEmail,
                    "mobileNumber": mobileNumber ==="" ? detailMobile : mobileNumber,
                    "address": address ==="" ? detailAdress : address,
                }
            }

        CoachService.updateCoach(coachId, updateCoach)
        window.location.reload();
    }

    const [tempFirstName, setTempFirstName] = useState("")
    const [tempSurName, setTempSurName] = useState("")
    const [tempCoachTeams, setTempCoachTeams] = useState([])
    const [tempCoachId, setTempCoachId] = useState("")
    const [detailEmail, setDetailEmail] = useState("");
    const [detailMobile, setDetailMobile] = useState("");
    const [detailAdress, setDetailAdress ] = useState("");

    const clickedCoach = (clickedFirstName, clickedSurName, clickedCoachTeams,clickedCoachId, clickedCoachMobile, clickedCoachAdress, clickedCoachEmail) => {

        setDetailEmail(clickedCoachEmail)
        setDetailMobile(clickedCoachMobile)
        setDetailAdress(clickedCoachAdress)

        setTempFirstName(clickedFirstName)
        setTempSurName(clickedSurName)
        setTempCoachTeams(clickedCoachTeams)
        setTempCoachId(clickedCoachId)
    }

    const selectTeam = (e) => {
        setAssignedTeam(e.target.value)
    }

    const assignTeam = () => {
        const teamId = parseInt(assignedTeam)
        const chosenTeam = teams.find((obj) => obj.id == teamId)
        const arrayCoachId = [tempCoachId]

        TeamService.assignCoachToTeam(teamId,arrayCoachId)

        const updateState = tempCoachTeams
        updateState.push(chosenTeam)
        setTeams(updateState)
    }

    return (
        <div className="coaches-main">
            <Modal closeTimeoutMS={500} isOpen={updateModalShow} onRequestClose={() => setUpdateModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '200px',
                           right: '200px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                <div className="modal-header-update">
                    <h1 className="modal-title">Update {tempFirstName}  {tempSurName}</h1>
                </div>

                    <div className="modal-body">
                    <div className="modal-form-update">
                        
                        <div className="update-input-div">
                        <p>Mobile Number</p>
                        <input onChange={onChangeMobileNumber} type="text" placeholder={detailMobile} className="modal-input-field-update"/>
                        </div>

                        <div className="update-input-div">
                        <p>Address</p>
                        <input onChange={onChangeAddress} type="text" placeholder={detailAdress} className="modal-input-field-update"/>
                        </div>

                        <div className="update-input-div">
                        <p>Email</p>
                        <input onChange={onChangeUpdateEmail} type="text" placeholder={detailEmail} className="modal-input-field-update"/>
                        </div>
                     
                    </div>
                    </div>
                    <div className="modal-footer">
                      <div className="modal-add-btn-update" onClick={submitUpdate} >Update</div>
                    </div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={assignTeamsModalShow} onRequestClose={() => setAssignTeamsModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.10)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '0px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '0px'
                       }
                   }}>
                <div className="modal-jersey-container">
                    <div className="modal-assign-header">
                        <h2> Assign Teams to {tempFirstName}  {tempSurName} </h2>
                    </div>
                    <div className="assign-coach-body">
                        <div className="coaches-teams">
                            <div className="coaches-teams-header">
                                <div className="teams-header-cont">
                                <h3>Current Teams</h3>
                                <img className="teams-image" width="25px" src={teamslogo}/>
                                </div>
                            </div>
                            <div className="coaches-teams-body">
                                {tempCoachTeams.map((team,index) => (
                                    <div className="teams-coach" key={index}>
                                        {team.name}
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="all-teams">
                            <div>
                            </div>
                            <div className="all-teams-image-container">
                                <div className="allteams-cont">
                                <img className="teams-image-big" src={teamslogo}/>
                                 </div>
                            </div>
                            <div className="select-div">
                                <select className="select-team" onChange={e => selectTeam(e)}>
                                    <option className="options" selected="selected" disabled="disabled">Choose team to assign</option>
                                    {teams.map((team,index) => (
                                        <option value={team.id} key={index}>
                                            {team.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="modal-jersey-footer">
                        <div onClick={() => assignTeam()} className="add-jerseynr">
                            Assign Team
                        </div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Coaches</h1>
                <img width="50px" className="runner" src={coachicon} />
            </div>
            <div className="coaches-list">
            <ul>
                {coaches.map((coach, index) => (
                <div className="coach-element" key={index}> 
                    
                    <div onClick={()=> { setAssignTeamsModalShow(true); clickedCoach(coach.user.firstName, coach.user.surName, coach.teams, coach.id,coach.user.mobileNumber, coach.user.address, coach.user.email)}} className="coach-name">
                        {coach.user.firstName} {coach.user.surName} 
                    </div>

                    {role.includes("Admin")?
                    <div className="delete-update">
                        <div onClick={() => {setCoachId(coach.id); setUpdateModalShow(true); clickedCoach(coach.user.firstName, coach.user.surName, coach.teams, coach.id,coach.user.mobileNumber, coach.user.address, coach.user.email); }} className="update">Update </div>
                        <div onClick={ () => deleteCoach(coach.user.id)} className="delete">Delete</div> 
                    </div>
                    :
                    <div className="delete-update-hide"> 

                    </div>
                    }
                     
                </div>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default Coaches
