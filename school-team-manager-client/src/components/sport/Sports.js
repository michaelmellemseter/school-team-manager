import { useState, useEffect } from "react";
import "../../css/list.css";
import Add from "@material-ui/icons/Add";
import Modal from "react-modal";
import AuthService from "../../services/AuthService";
import SportService from "../../services/SportService";
import Loader from "../shared/loader/Loader";
import sportsicon from "../../assets/svg/sportsicon.svg";
import NotificationService from "../../services/NotificationService";
import NotifyModal from "../shared/notifymodal/NotifyModal";

function Sports() {

  const role = AuthService.getRole()
  const [sports, setSports] = useState([]);
  const [sportId, setSportId] = useState("");
  const [name, setName] = useState("");
  const [updateName, setUpdateName] = useState("");

  const [modalShow, setModalShow] = useState(false);
  const [updateModalShow, setUpdateModalShow] = useState(false);
  const [deleteModalShow, setDeleteModalShow] = useState(false);
  const [notifyModalShow, setNotifyModalShow] = useState(false);

  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getAllSports();
    setLoading(false);
  }, []);

  const getAllSports = () => {
    SportService.getAllSports()
      .then((response) => {
        setSports(response.data);
      })
      .catch((error) => console.error(error.message));
  };

  const deleteSport = (id) => {
    setDeleteModalShow(false);
    SportService.deleteSport(id);

    const updateState = sports.filter(function (value, index, arr) {
      return value.id !== id;
    });
    setSports(updateState);
  };

  const onChangeName = (event) => {
    setName(event.target.value);
  };

  const newSportForm = () => {
    if (name.length === 0) {
      setErrorMessage("You need to fill all the fields");
    } else {
      setModalShow(false);

      const newSport = {
        name: name,
      };
      SportService.addSport(newSport);

      const updateState = sports;
      updateState.push(newSport);
      setSports(updateState);
    }
  };

  const onChangeUpdateName = (event) => {
    setUpdateName(event.target.value);
  };

  const setUpdateDetails = (id, name) => {
    setSportId(id);
    setUpdateName(name);
  };

  const submitUpdate = () => {
    if (updateName.length === 0) {
      setErrorMessage("You need to fill all the fields");
    } else {
      setUpdateModalShow(false);

      const updateSport = {
        id: sportId,
        name: updateName,
      };
      SportService.updateSport(sportId, updateSport);

      let updateState = sports;
      for (let sport in updateState) {
        if (updateState[sport].id === sportId) {
          updateState[sport] = updateSport;
          break;
        }
      }
      setSports(updateState);
    }
  };

  return (
    <div className="list-main">
      <Modal
        closeTimeoutMS={500}
        isOpen={modalShow}
        onRequestClose={() => setModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Add New Sport</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <input
                onChange={onChangeName}
                type="text"
                placeholder="Name"
                className="modal-input-field"
              />
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={newSportForm}>
              Add Sport
            </div>
          </div>
          <div className="modal-error-message">{errorMessage}</div>
        </div>
      </Modal>
      <Modal
        closeTimeoutMS={500}
        isOpen={updateModalShow}
        onRequestClose={() => setUpdateModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Update Sport</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <input
                onChange={onChangeUpdateName}
                type="text"
                placeholder={updateName}
                className="modal-input-field"
              />
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={submitUpdate}>
              Update Sport
            </div>
          </div>
          <div className="modal-error-message">{errorMessage}</div>
        </div>
      </Modal>
      <Modal
        closeTimeoutMS={500}
        isOpen={deleteModalShow}
        onRequestClose={() => setDeleteModalShow(false)}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.50)",
          },
          content: {
            position: "absolute",
            top: "100px",
            left: "400px",
            right: "400px",
            bottom: "100px",
            border: "1px solid #ccc",
            background: "#242424",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container">
          <div className="modal-header">
            <h1 className="modal-title">Delete Sport</h1>
          </div>
          <div className="modal-body">
            <div className="modal-form">
              <p className="team-p">
                Are you sure you want to delete this sport?
              </p>
            </div>
          </div>
          <div className="modal-footer">
            <div className="modal-add-btn" onClick={() => deleteSport(sportId)}>
              Delete Sport
            </div>
          </div>
        </div>
      </Modal>
      <NotifyModal
        showing={notifyModalShow}
        closeFunction={setNotifyModalShow}
        receiverId={sportId}
        sendFunction={NotificationService.postNotificationSport}
      />
      <div className="title">
        <h1>Sports</h1>
        <img width="60px" className="runner" src={sportsicon} />
      </div>
      <div className="list">
        {loading ? (
          <div>
            <Loader />
          </div>
        ) : (
          <ul>
            {(role.includes("Admin") || role.includes("Coach")) && (
              <div className="list-add-btn" onClick={() => setModalShow(true)}>
                <Add fontSize="large" />{" "}
                <p className="add-new-text">Add New Sport</p>
              </div>
            )}
            {sports.map((sport, index) => (
              <div className="list-element" key={index}>
                <div className="list-name">{sport.name}</div>
                {(role.includes("Admin") || role.includes("Coach")) && (
                  <div className="delete-update">
                    <div
                      onClick={() => {
                        setUpdateDetails(sport.id, sport.name);
                        setUpdateModalShow(true);
                      }}
                      className="update"
                    >
                      Update
                    </div>
                    <div
                      onClick={() => {
                        setSportId(sport.id);
                        setDeleteModalShow(true);
                      }}
                      className="delete"
                    >
                      Delete
                    </div>
                    {role.includes("Coach") && (
                      <div
                        onClick={() => {
                          setSportId(sport.id);
                          setNotifyModalShow(true);
                        }}
                        className="update"
                      >
                        Notify parents
                      </div>
                    )}
                  </div>
                )}
              </div>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
}

export default Sports;
