import { useState,useEffect } from "react"
import "../../css/list.css"
import MessageToAdminService from "../../services/MessageToAdminService";
import Modal from "react-modal";
import Loader from "../shared/loader/Loader";
import messageicon from "../../assets/svg/messageicon.svg";
function MessagesToAdmin () {

    const [messages, setMessages] = useState([])
    const [messageId, setMessageId] = useState("")

    const [detailTitle, setDetailTitle] = useState("")
    const [detailMessage, setDetailMessage] = useState("")
    const [detailUser, setDetailUser] = useState("")
    const [detailsShow, setDetailsShow] = useState(false)

    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        getAllMessages()
        setLoading(false)
    }, []);

    const getAllMessages = () => {
        MessageToAdminService.getAllActiveMessages()
            .then((response) => {
                setMessages(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const setDetails = (id, title, message, user) => {
        setMessageId(id)
        setDetailTitle(title)
        setDetailMessage(message)
        setDetailUser(user)
    }

    const approveMessage = (approved) => {
        setDetailsShow(false)

        const updateMessage = {
            "id": messageId,
            "approved": approved
        }
        MessageToAdminService.approve(messageId, updateMessage)

        const updateState = messages.filter(function(value, index, arr) {
            return value.id !== messageId
        })
        setMessages(updateState)
    }

    return (
        <div className="list-main">
            <div className="nav">
            </div>
            <Modal closeTimeoutMS={500} isOpen={detailsShow} onRequestClose={() => setDetailsShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">{detailTitle}</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            {detailUser.email}
                            <br/>
                            <br/>
                            {detailMessage}
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={ () => approveMessage(true)}>Approve</div>
                        <div className="modal-add-btn" onClick={ () => approveMessage(false)}>Deny</div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Messages/Corrections</h1>
                <img width="50px" className="runner" src={messageicon} />
            </div>
            <div className="list">
                {loading ? (
                    <div>
                        <Loader />
                    </div>
                ) : (
                    <ul>
                        {messages.map((message, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name" onClick={() => { setDetails(message.id, message.title, message.message, message.user) ; setDetailsShow(true) }}>
                                    {message.title}
                                </div>
                            </div>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    )
}

export default MessagesToAdmin
