import { NavLink } from "react-router-dom";
import "../../css/navbar.css";
import { ROUTES } from "../../constants/routes";
import AuthService from "../../services/AuthService";

function Navbar() {
  const token = AuthService.getToken();
  const role = token ? AuthService.getRole() : null;
  const name = token ? AuthService.getName() : null;

  const logOut = () => {
    AuthService.doLogout();
  };

  return (
    <nav className="navbar">
      {token && (
        <ul className="nav-list">
          <li className="nav-element">
            <NavLink
              exact
              activeClassName="active"
              to={ROUTES.Home}
              className="styled-link  first"
            >
              Home
            </NavLink>
          </li>

          {(role.includes("Admin") || role.includes("Coach")) && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.Users}
                className="styled-link"
              >
                Users
              </NavLink>
            </li>
          )}

          {role.includes("Admin") && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.Coaches}
                className="styled-link"
              >
                Coaches
              </NavLink>
            </li>
          )}

          {(role.includes("Admin") || role.includes("Coach")) && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.Parents}
                className="styled-link"
              >
                Parents
              </NavLink>
            </li>
          )}

          <li className="nav-element">
            <NavLink
              exact
              activeClassName="active"
              to={ROUTES.Players}
              className="styled-link"
            >
              Players
            </NavLink>
          </li>

          <li className="nav-element">
            <NavLink
              exact
              activeClassName="active"
              to={ROUTES.UpcomingMatches}
              className="styled-link"
            >
              Matches
            </NavLink>
          </li>

          <li className="nav-element">
            <NavLink
              exact
              activeClassName="active"
              to={ROUTES.FinishedMatches}
              className="styled-link"
            >
              Finished matches
            </NavLink>
          </li>
          {(role.includes("Admin") ||
            role.includes("Coach") ||
            role.includes("Player")) && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.TeamsMain}
                className="styled-link"
              >
                Teams
              </NavLink>
            </li>
          )}

          {role.includes("Admin") && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.Schools}
                className="styled-link"
              >
                Schools
              </NavLink>
            </li>
          )}
          {(role.includes("Admin") || role.includes("Coach")) && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.Sports}
                className="styled-link"
              >
                Sports
              </NavLink>
            </li>
          )}
          {(role.includes("Admin") || role.includes("Coach")) && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.Locations}
                className="styled-link"
              >
                Locations
              </NavLink>
            </li>
          )}

          {role.includes("Admin") && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.LocationTypes}
                className="styled-link"
              >
                Location Types
              </NavLink>
            </li>
          )}
          {role.includes("Admin") && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.MessagesToAdmin}
                className="styled-link"
              >
                Messages
              </NavLink>
            </li>
          )}
          {(role.includes("Coach") ||
            role.includes("Parent") ||
            role.includes("Player")) && (
            <li className="nav-element">
              <NavLink
                exact
                activeClassName="active"
                to={ROUTES.SendMessageToAdmin}
                className="styled-link"
              >
                Send message
              </NavLink>
            </li>
          )}
          <div className="last">
            {role.includes("Parent") && (
                <li className="nav-element">
                  <NavLink
                      exact
                      activeClassName="active"
                      to={ROUTES.Notifications}
                      className="styled-link"
                  >
                    Notifications
                  </NavLink>
                </li>
            )}
            {(role.includes("Coach") ||
              role.includes("Parent") ||
              role.includes("Player")) && (
              <li className="nav-element">
                <NavLink
                  exact
                  activeClassName="active"
                  to={ROUTES.ProfileMain}
                  className="styled-link"
                >
                  {name}
                </NavLink>
              </li>
            )}{" "}
            <li className="nav-element">
              <div onClick={logOut} className="styled-link">
                Log out
              </div>
            </li>
          </div>
        </ul>
      )}
    </nav>
  );
}

export default Navbar;
