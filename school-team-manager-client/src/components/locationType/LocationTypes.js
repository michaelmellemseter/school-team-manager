import { useState,useEffect } from "react"
import "../../css/list.css"
import Add from "@material-ui/icons/Add";
import Modal from "react-modal";
import AuthService from "../../services/AuthService"
import LocationTypeService from "../../services/LocationTypeService";
import locationtypeicon from "../../assets/svg/locationtypeicon.svg"
import Loader from "../shared/loader/Loader";

function LocationTypes () {

    const role = AuthService.getRole()
    const [locationTypes, setLocationTypes] = useState([])
    const [locationTypeId, setLocationTypeId] = useState("")
    const [name, setName] = useState("")
    const [updateName, setUpdateName] = useState("")

    const [modalShow, setModalShow] = useState(false)
    const [updateModalShow, setUpdateModalShow] = useState(false)
    const [deleteModalShow, setDeleteModalShow] = useState(false)

    const [errorMessage, setErrorMessage] = useState("")
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        getAllLocationTypes()
        setLoading(false)
    }, []);

    const getAllLocationTypes = () => {
        LocationTypeService.getAllLocationTypes()
            .then((response) => {
                setLocationTypes(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const deleteLocationType = (id) => {
        setDeleteModalShow(false)
        LocationTypeService.deleteLocationType(id)

        const updateState = locationTypes.filter(function(value, index, arr) {
            return value.id !== id
        })
        setLocationTypes(updateState)
    }

    const onChangeName = event => {
        setName(event.target.value)
    }

    const newLocationTypeForm = () => {
        if (name.length === 0) {
            setErrorMessage("You need to fill all the fields")
        }
        else {
            setModalShow(false)

            const newLocationType =
                {
                    "name": name
                }
            LocationTypeService.addLocationType(newLocationType)

            const updateState = locationTypes
            updateState.push(newLocationType)
            setLocationTypes(updateState)
        }
    }

    const onChangeUpdateName = event => {
        setUpdateName(event.target.value)
    }

    const setUpdateDetails = (id, name) => {
        setLocationTypeId(id)
        setUpdateName(name)
    }

    const submitUpdate = () => {
        if (updateName.length === 0) {
            setErrorMessage("You need to fill all the fields")
        } else {
            setUpdateModalShow(false)

            const updateLocationType =
                {
                    "id": locationTypeId,
                    "name": updateName
                }
            LocationTypeService.updateLocationType(locationTypeId, updateLocationType)

            let updateState = locationTypes
            for (let locType in updateState) {
                if (updateState[locType].id === locationTypeId) {
                    updateState[locType] = updateLocationType
                    break
                }
            }
            setLocationTypes(updateState)
        }
    }

    return (
        <div className="list-main">
            <Modal closeTimeoutMS={500} isOpen={modalShow} onRequestClose={() => setModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Add New Location Type</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeName} type="text" placeholder="Name" className="modal-input-field"/>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={newLocationTypeForm}>Add Location Type</div>
                    </div>
                    <div className="modal-error-message">{errorMessage}</div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={updateModalShow} onRequestClose={() => setUpdateModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Update Location Type</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeUpdateName} type="text" placeholder={updateName} className="modal-input-field"/>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={submitUpdate}>Update Location Type</div>
                    </div>
                    <div className="modal-error-message">{errorMessage}</div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={deleteModalShow} onRequestClose={() => setDeleteModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Delete Location Type</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <p className="team-p">Are you sure you want to delete this location type?</p>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={ () => deleteLocationType(locationTypeId)}>Delete Location Type</div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Location Types</h1>
                <img width="50px" className="runner" src={locationtypeicon} />
            </div>
            <div className="list">
                {loading ? (
                    <div>
                        <Loader />
                    </div>
                ) : (
                    <ul>
                        {role.includes("Admin") ?
                            <div className="list-add-btn" onClick={() => setModalShow(true)}>
                                <Add fontSize="large"/> <p className="add-new-text">Add New Location Type</p>
                            </div>
                            :
                            <div className ="hide-add-btn">
                            </div>
                        }

                        {locationTypes.map((locationType, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name">
                                    {locationType.name}
                                </div>
                                {role.includes("Admin") ?
                                    <div className="delete-update">
                                        <div onClick={() => {setUpdateDetails(locationType.id, locationType.name); setUpdateModalShow(true); }} className="update">Update</div>
                                        <div onClick={ () => {setLocationTypeId(locationType.id) ; setDeleteModalShow(true)}} className="delete">Delete</div>
                                    </div>
                                    :
                                    <div className="delete-update-hide">
                                    </div>
                                }
                            </div>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    )
}

export default LocationTypes
