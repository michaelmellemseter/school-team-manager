import Profile from "./Profile"
import ParentProfile from "./ParentProfile"
import AuthService from "../../services/AuthService"
import { useState } from "react"
import { useEffect } from "react"
import CoachProfile from "./CoachProfile"

function ProfileMain() {


    useEffect(() => {
        getRole();
      }, []);

    const [role, setRole] = useState("")

    const getRole = () => {
        const role = AuthService.getRole(); 
        console.log(role) ; 
        setRole(role); 

    }


    return(
        <div>
            {role.includes("Parent") ? 
            <ParentProfile/>
            : role.includes("Player")?
            <Profile/>
            : role.includes("Coach") ?
            <CoachProfile/>
            :
            <p>no valid user</p>
            }
            {/* <ParentProfile/> */}
        </div>
        

    )

}

export default ProfileMain