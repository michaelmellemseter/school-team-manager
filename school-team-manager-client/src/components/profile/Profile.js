import { useEffect, useState } from "react";
import AuthService from "../../services/AuthService"
import UserService from "../../services/UserService"
import "../../css/profile.css"
import HttpService from "../../services/HttpService"
import injury from "../../assets/svg/injury.svg"
import personalia from "../../assets/svg/personalia.svg"
import runner from "../../assets/svg/runner.svg"
// import "../../css/coaches.css"


function Profile() {

    const PLAYER_PRIVACY =  "http://localhost:8080/api/v1/players/privacy/";
    const userId = AuthService.getDbId();
    const [user, setUser] = useState("")
    const [player, setPlayer] = useState("")


    const [personaliaChecked , setPersonaliaChecked] = useState(false)
    const [photoChecked , setPhotoChecked] = useState(false)
    const [medicalChecked , setMedicalChecked] = useState(false)

    useEffect(()=> {
        
        getUserById(userId)
        getPlayerByUserId(userId)
        console.log(personaliaChecked)
        
    },[])

    useEffect (()=> {
        // setPersonaliaChecked(!personaliaChecked)
        updatePrivacy();
    },[personaliaChecked,photoChecked, medicalChecked])

    const getUserById = (id) => {

        // UserService.getUserById(userId)
        UserService.getUserById(id)
        .then((response) => {
            console.log(response.data)
            const user = response.data; 
            setUser(user); 

        }).catch(error=>console.error("error, no user"))

    }

    const getPlayerByUserId = (id) => {
        UserService.getPlayerByUserId(id)
        .then((response) => {
            console.log(response.data) 
            const player = response.data; 
            setPlayer(player);
            setPersonaliaChecked(player.showPersonalia)
            setMedicalChecked(player.showMedicalNotes) 
            setPhotoChecked(player.showPhoto)
        })
    }

    //userid 28, playerid10

    const handleChangePersonalia = () => {
       
        setPersonaliaChecked(!personaliaChecked)
  
        updatePrivacy();
    }

    const handlePhotoChecked = () => {
        setPhotoChecked(!photoChecked)
        updatePrivacy();
    }

    const handleChangeInjury = () => {
        setMedicalChecked(!medicalChecked)
        updatePrivacy();
    }

    const updatePrivacy = () => {
        console.log(player.id)
        
        const updatetPrivacy = {
            "id" : player.id,
            "showPersonalia" : personaliaChecked,
            "showPhoto" : photoChecked,
            "showMedicalNotes": medicalChecked,
            
        }

        
        const axios = HttpService.getAxiosClient(); 
        
        axios.patch("https://school-team-manager.herokuapp.com/api/v1/players/privacy/"+player.id, updatetPrivacy)

    }

    return(
        <div className="profile-player-container">

            <div className="profile-header">
               <h1 className="profile-title">{user.firstName} {user.surName}</h1>
               <img id="runner-header" width="70px" src={runner}/>
            </div>
            
            <div className="profile-body">
                <div className="profile-body-left">
                  <div className="profile-image-container">
                      <img id="profile-pic-player" src={player.profilePicture}/>
                      <div id="input-photo" className="input-section">
                         <p>display?</p>
                         <input checked={photoChecked} onChange={ () => handlePhotoChecked()}  className="checkbox" type="checkbox" />
                      </div>
                  </div>
                </div>

                <div className="profile-body-right">
                <div className="personal-info">
                    <div className="personal-header">
                        
                        <div className="personalia-left-header">
                        <img src={personalia} width="40px" />
                        <h3>Personalia</h3>
                        </div>
                        
                        <div className="check-display">

                            <p className="display"> display?</p> 
                            <input checked={personaliaChecked} onChange={ () => handleChangePersonalia()}  className="checkbox" type="checkbox" />

                        </div>
                    </div>
                    <div className="personal-data">
                        <p> {user.mobileNumber}</p>
                        <p> {user.date}</p>
                        <p> {user.email}</p>
                        <p> {user.address}</p>
                        <p> {personaliaChecked} </p>
                    </div>
               

                </div>
                
                <div className="profile-injury-section">
                    <div className="profile-injury-header">
                        <div className="injury-header-left">
                            <img className="image-profile-injury" src={injury} width="30px"/>
                            <h3>Injury Status</h3>
                        </div>
                            
                        <div className="input-section">
                            <p > display?</p> 
                            <input checked={medicalChecked} onChange={ () => handleChangeInjury()} id="check"  className="checkbox" type="checkbox" />
                        </div>
                    </div>

                    <div className="profile-injury-body">
                        <p> {player.medicalNotes} </p>
                    </div> 

                    </div>
                </div>

                </div>


                
            </div>
            
        
    )

}

export default Profile; 