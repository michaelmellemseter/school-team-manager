import HttpService from "./HttpService"

const SCHOOL_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/schools`
const axios = HttpService.getAxiosClient()

class SchoolService {
    getAllSchools() {
        return axios.get(SCHOOL_REST_API_URL)
    }
    addSchool(school) {
        axios.post(SCHOOL_REST_API_URL, school)
    }
    updateSchool(id, school) {
        axios.put(`${SCHOOL_REST_API_URL}/${id}`, school)
    }
    deleteSchool(id) {
        axios.delete(`${SCHOOL_REST_API_URL}/${id}`)
    }
}

export default new SchoolService()
