import HttpService from "./HttpService";

const PLAYER_REST_API_URL = "https://school-team-manager.herokuapp.com/api/v1/players";
const axios = HttpService.getAxiosClient();

class PlayerService {
  getAllPlayers() {
    return axios.get(PLAYER_REST_API_URL);
  }
  getPlayerByURL(url) {
    return axios.get(`http://localhost:8080${url}`);
  }
  addPlayer(player) {
    axios.post(PLAYER_REST_API_URL, player);
  }
  updatePlayerPartially(id, player) {
    axios.patch(PLAYER_REST_API_URL + "/" + id, player);
  }
  // Get matches, either previous or upcoming (based on query param time)
  getMatches(id, time) {
    return axios.get(`${PLAYER_REST_API_URL}/${id}/matches?time=${time}`);
  }
  addParentToPlayer(playerId, parentId) {
    console.log("playerId " + playerId);
    axios.patch(PLAYER_REST_API_URL + "/" + playerId + "/parents", parentId);
  }

  getTeamsByPlayerId(playerId) {
   return axios.get(PLAYER_REST_API_URL+"/"+playerId+"/teams")
  }


}

export default new PlayerService();
