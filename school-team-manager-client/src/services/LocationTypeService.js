import HttpService from "./HttpService"

const LOCATION_TYPE_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/locations/types`
const axios = HttpService.getAxiosClient()

class LocationTypeService {
    getAllLocationTypes() {
        return axios.get(LOCATION_TYPE_REST_API_URL)
    }
    addLocationType(locationType) {
        axios.post(LOCATION_TYPE_REST_API_URL, locationType)
    }
    updateLocationType(id, locationType) {
        axios.put(`${LOCATION_TYPE_REST_API_URL}/${id}`, locationType)
    }
    deleteLocationType(id) {
        axios.delete(`${LOCATION_TYPE_REST_API_URL}/${id}`)
    }
}

export default new LocationTypeService()
