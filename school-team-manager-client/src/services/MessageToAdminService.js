import HttpService from "./HttpService"

const MESSAGE_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/admins/messages`
const axios = HttpService.getAxiosClient()

class MessageToAdminService {
    getAllActiveMessages() {
        return axios.get(`${MESSAGE_REST_API_URL}/active`)
    }
    addMessage(message) {
        axios.post(MESSAGE_REST_API_URL, message)
    }
    approve(id, message) {
        axios.patch(`${MESSAGE_REST_API_URL}/${id}/approve`, message)
    }
}

export default new MessageToAdminService()
