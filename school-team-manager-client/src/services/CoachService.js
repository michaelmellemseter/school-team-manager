import HttpService from "./HttpService";

const COACH_REST_API_URL = "https://school-team-manager.herokuapp.com/api/v1/coaches";
const axios = HttpService.getAxiosClient();

class CoachService {
  getAllCoaches() {
    return axios.get(COACH_REST_API_URL);
  }
  updateCoach(id, coach) {
    axios.patch(COACH_REST_API_URL + "/" + id, coach);
  }
  // Get matches, either previous or upcoming (based on query param time)
  getMatches(id, time) {
    return axios.get(`${COACH_REST_API_URL}/${id}/matches?time=${time}`);
  }
}

export default new CoachService();
