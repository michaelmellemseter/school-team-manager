import HttpService from "./HttpService"

const TEAM_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/teams`
const axios = HttpService.getAxiosClient()

class TeamService {
    getAllTeams() {
        return axios.get(TEAM_REST_API_URL)
    }
    addTeam(team) {
        return axios.post(TEAM_REST_API_URL, team)
    }
    updateTeam(id, team) {
        axios.put(`${TEAM_REST_API_URL}/${id}`, team)
    }
    deleteTeam(id) {
        axios.delete(`${TEAM_REST_API_URL}/${id}`)
    }
    getPlayersByTeamId(id) {
        return axios.get(`${TEAM_REST_API_URL}/${id}`+"/players")
    }
    addPlayerToTeam(teamId, player) {
        axios.patch( `${TEAM_REST_API_URL}/${teamId}`+"/players", player)
    }
    deletePlayerFromTeam(teamId, playerId) {
        axios.delete(`${TEAM_REST_API_URL}/${teamId}`+"/players/"+ playerId)
    }
    assignCoachToTeam(teamId,coachId) {
        axios.patch(`${TEAM_REST_API_URL}/${teamId}`+"/coaches",coachId)
    }
}

export default new TeamService()
