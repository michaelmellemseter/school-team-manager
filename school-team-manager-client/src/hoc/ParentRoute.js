import { Redirect, Route } from 'react-router-dom'
import AuthService from "../services/AuthService";
import { ROUTES } from "../constants/routes";

export const ParentRoute = props => {
    const role = AuthService.getRole();

    if(role === "Parent") return <Route {...props} />

    return <Redirect to={ ROUTES.LandingPage } />
}

export default ParentRoute
