import { Redirect, Route } from 'react-router-dom'
import AuthService from "../services/AuthService";
import { ROUTES } from "../constants/routes";

export const StartRoute = props => {
    const authenticated = AuthService.isLoggedIn();

    // If user is not logged in, then redirect user to the start page
    if(authenticated) return <Redirect to={ ROUTES.Home } />

    return <Route {...props} />
}

export default StartRoute
