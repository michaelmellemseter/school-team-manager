import { Redirect, Route } from 'react-router-dom'
import AuthService from "../services/AuthService";
import { ROUTES } from "../constants/routes";

export const AdminCoachRoute = props => {
    const role = AuthService.getRole();

    if(role === "Admin" || role === "Coach") return <Route {...props} />

    return <Redirect to={ ROUTES.LandingPage } />
}

export default AdminCoachRoute
